package com.example.project.fragment.payment

import android.content.Context
import android.content.DialogInterface
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.currentCart
import com.example.project.http.API
import com.example.project.model.Cart
import com.example.project.model.Customer
import com.example.project.model.Product
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody

class VMPaymentFragment : ViewModel() {
    lateinit var context: Context
    lateinit var loadingView: MutableLiveData<Int>
    lateinit var paySuccess: MutableLiveData<Boolean>
    lateinit var body: HashMap<String, String>
    lateinit var newCart: MutableLiveData<Cart>
    lateinit var currentCustomer: MutableLiveData<Customer>
    lateinit var currentProduct: MutableLiveData<Product>

    fun init(context: Context) {
        this.context = context
        loadingView = MutableLiveData(View.GONE)
        paySuccess = MutableLiveData()
        newCart = MutableLiveData()
        currentCustomer = MutableLiveData()
        currentProduct = MutableLiveData()
    }

    fun pay(quote_id: String, point: String) {
        loadingView.value = View.VISIBLE
        if (point != "") {
            if (point.toInt() < 0) {
                body = hashMapOf(
                    "quote_id" to quote_id
                )
            } else {
                body = hashMapOf(
                    "quote_id" to quote_id,
                    "point" to point
                )
            }
        }

            API.apiService.pay(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    loadingView.value = View.GONE
                    if (it.status == "success") {
                        MessageBox().showAlert(
                            context,
                            it.message,
                            context.getString(R.string.ok),
                            object : DialogInterface.OnClickListener {
                                override fun onClick(p0: DialogInterface?, p1: Int) {
                                    paySuccess.value = true
                                }
                            })
                    } else {
                        MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                    }
                }, {
                    loadingView.value = View.GONE
                    LogUtil.debug("__err_pay__", it.toString())
                    MessageBox().showAlert(
                        context,
                        context.getString(R.string.err_internet),
                        context.getString(R.string.ok)
                    )
                }).isDisposed
    }

    fun deleteQuoteItem(quote_item_id: String, customer_id: String) {
        loadingView.value = View.VISIBLE
        if (customer_id != "") {
            body = hashMapOf(
                "customer_id" to customer_id,
                "quote_id" to currentCart!!.id.toString(),
                "quote_item_id" to quote_item_id
            )
        } else {
            body = hashMapOf(
                "quote_id" to currentCart!!.id.toString(),
                "quote_item_id" to quote_item_id
            )
        }

        API.apiService.deleteQuoteItem(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    currentCart = it.result
                    MessageBox().showLongToast(context, it.message)
                    newCart.value = it.result
                } else {
                    MessageBox().showAlert(
                        context,
                        it.message,
                        context.getString(R.string.ok)
                    )
                }
            }, {
                loadingView.value = View.GONE
                LogUtil.debug("__err_delete_quote_item__", it.toString())
                MessageBox().showAlert(
                    context,
                    context.getString(R.string.err_internet),
                    context.getString(R.string.ok)
                )
            }).isDisposed
    }

    fun getCustomer(customer_phone: String) {
        loadingView.value = View.VISIBLE
        body = hashMapOf(
            "page" to "1",
            "customer_name" to "",
            "customer_phone" to customer_phone
        )

        API.apiService.getListCustomer(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    if (it.result != null) {
                        if (it.result!!.size > 0) {
                            currentCustomer.value = it.result!!.get(0)
                        }
                    }
                } else {
                    MessageBox().showLongToast(context, it.message)
                }
            }, {
                loadingView.value = View.GONE
                LogUtil.debug("__err_get_list_cus__", it.toString())
                MessageBox().showLongToast(context, context.getString(R.string.err_internet))
            }).isDisposed
    }

    fun addCustomer(customer_name: String, customer_phone: String) {
        loadingView.value = View.VISIBLE

        val c_customer_name = RequestBody.create(MediaType.parse("text/plain"), customer_name)
        val c_customer_phone = RequestBody.create(MediaType.parse("text/plain"), customer_phone)

        API.apiService.addCustomer(
            c_customer_name,
            c_customer_phone
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    currentCustomer.value = it.result
                    MessageBox().showLongToast(context, it.message)
                } else MessageBox().showLongToast(context, it.message)
            }, {
                loadingView.value = View.GONE
                LogUtil.debug("__err_add_cus__", it.toString())
                MessageBox().showLongToast(context, context.getString(R.string.err_internet))
            }).isDisposed
    }

    fun addToCart(
        customer_id: String,
        product_id: String,
        product_qty: String,
        note: String
    ) {
        loadingView.value = View.VISIBLE
        if (customer_id != "") {
            body = hashMapOf(
                "customer_id" to customer_id,
                "product_id" to product_id,
                "product_qty" to product_qty,
                "note" to note
            )
        } else {
            body = hashMapOf(
                "product_id" to product_id,
                "product_qty" to product_qty,
                "note" to note
            )
        }

        API.apiService.addToCart(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    if (it.result != null) {
                        MessageBox().showLongToast(context, it.message)
                        currentCart = it.result!!
                        newCart.value = it.result!!
                    }
                } else {
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                }
            }, {
                loadingView.value = View.GONE
                MessageBox().showLongToast(context, context.getString(R.string.err_internet))
                LogUtil.debug("__add_to_cart_none_customer__", it.toString())
            }).isDisposed
    }

    fun getProduct(code: String) {
        loadingView.value = View.VISIBLE
        body = hashMapOf(
            "page" to "1",
            "product_name" to "",
            "code" to code
        )
        LogUtil.debug("__code__", code)
        API.apiService.getListProduct(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    if (it.result != null) {
                        if (it.result!!.size > 0) {
                            currentProduct.value = it.result!!.get(0)
                        } else {
                            newCart.value = currentCart
                            MessageBox().showLongToast(context, "Không tìm thấy sản phẩm")
                        }
                    }
                } else {
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                }
            }, {
                loadingView.value = View.GONE
                MessageBox().showAlert(
                    context,
                    context.getString(R.string.err_internet),
                    context.getString(R.string.ok)
                )
                LogUtil.debug("__err_get_list_product__", it.toString())
            }).isDisposed
    }
}