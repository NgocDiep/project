package com.example.project.fragment.revuene

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.project.R
import com.example.project.base.BaseFragment
import com.example.project.navigation.MyNavigation
import com.example.project.utils.MessageBox
import com.example.project.utils.RecyclerItemCLickListener
import com.example.project.utils.setOnLoadMore
import kotlinx.android.synthetic.main.fragment_revuene.*

/**
 * A simple [Fragment] subclass.
 */
class RevueneFragment : BaseFragment() {
    lateinit var revueneAdapter: RevueneAdapter
    private var pageIndex = 1
    private var customer_name = "all_order"
    lateinit var viewModel: VMRevueneFragment
    lateinit var layoutManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_revuene, container, false)
    }

    override fun onResume() {
        super.onResume()
        loadAll()
    }

    fun loadAll() {
        pageIndex = 1
        init()
        event()
        fetchData()
        dataListener()
    }

    fun init() {
        viewModel = ViewModelProviders.of(this@RevueneFragment).get(VMRevueneFragment::class.java)
        viewModel.init(context!!)

        revueneAdapter = RevueneAdapter(context!!)
        layoutManager = LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
        rv_history_order.adapter = revueneAdapter
        rv_history_order.layoutManager = layoutManager
    }

    fun event() {
        edt_search.setOnClickListener {
            ll_clear_filter.visibility = View.VISIBLE
            btn_search.setOnClickListener {
                if (edt_search.text.toString() == "") {
                    MessageBox().showLongToast(
                        context!!,
                        context!!.getString(R.string.require_input_key)
                    )
                } else {
                    customer_name = edt_search.text.toString()
                    loadAll()
                }
            }
            btn_clear_filter.setOnClickListener {
                customer_name = "all_order"
                edt_search.setText("")
                loadAll()
                ll_clear_filter.visibility = View.GONE
            }
        }

        rv_history_order.addOnItemTouchListener(RecyclerItemCLickListener(
            context!!,
            rv_history_order,
            object : RecyclerItemCLickListener.ClickListener {
                override fun onClick(view: View, position: Int) {
                    viewModel.getHistoryOrderDetail(viewModel.listHistoryOrder.value!![position].id)
                }

                override fun onLongClick(view: View?, position: Int) {
                }
            }
        ))

        rv_history_order.setOnLoadMore {
            if (viewModel.listHistoryOrder.value!!.size >= 10) {
                pageIndex++
                viewModel.getListHistoryOrder(pageIndex.toString(), customer_name)
            }
        }
    }

    fun fetchData() {
        viewModel.getListHistoryOrder(pageIndex.toString(), customer_name)
    }

    fun dataListener() {
        viewModel.loadingView.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                showLoading()
            } else dismissLoading()
        })

        viewModel.listHistoryOrder.observe(viewLifecycleOwner, Observer {
            if (it.size == 0) {
                tv_no_data.visibility = View.VISIBLE
                rv_history_order.visibility = View.GONE
            } else {
                revueneAdapter.refresh(it)
            }
        })

        viewModel.getOrderDetailSuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                MyNavigation.getInstance().goToHistoryOrderDetail()
            }
        })
    }
}
