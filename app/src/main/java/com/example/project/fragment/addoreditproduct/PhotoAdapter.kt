package com.example.project.fragment.addoreditproduct

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.project.R
import kotlinx.android.synthetic.main.layout_item_image_product.view.*

class PhotoAdapter(val context: Context) : RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder>() {
    val listImagePath = ArrayList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.layout_item_image_product, parent, false)
        return PhotoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listImagePath.size
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bindView(listImagePath.get(position))
    }

    fun append(path: String) {
        this.listImagePath.add(path)
        this.notifyDataSetChanged()
    }

    fun refresh(list: List<String>) {
        this.listImagePath.clear()
        this.listImagePath.addAll(list)
        this.notifyDataSetChanged()
    }

    fun deleteItem(path: String) {
        this.listImagePath.remove(path)
        this.notifyDataSetChanged()
    }

    fun deleteAll() {
        this.listImagePath.clear()
        this.notifyDataSetChanged()
    }

    inner class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(item: String) {
//            itemView.image.setBackgroundResource()
            Glide.with(itemView).load(item).error(R.drawable.ic_logo)
                .placeholder(R.drawable.ic_logo)
                .into(itemView.image)

            itemView.btn_delete.setOnClickListener {
                deleteItem(item)
            }
        }
    }
}