package com.example.project.fragment.addoreditproduct

import android.content.Context
import android.content.DialogInterface
import android.net.Uri
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.currentProduct
import com.example.project.http.API
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import com.example.project.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class VMAddOrEditProductFragment : ViewModel() {
    lateinit var loadingView: MutableLiveData<Int>
    lateinit var context: Context
    lateinit var addSuccess: MutableLiveData<Boolean>
    lateinit var editSuccess: MutableLiveData<Boolean>
    var listFile = ArrayList<MultipartBody.Part>()

    fun init(context: Context) {
        this.loadingView = MutableLiveData(View.GONE)
        this.context = context
        this.addSuccess = MutableLiveData()
        this.editSuccess = MutableLiveData()
    }

    fun addProduct(
        name: String, code: String, qty: String, price: String, discount_rate: String,
        unit: String, listPath: List<String>
    ) {
        if (Utils.isNullOrEmpty(name) || Utils.isNullOrEmpty(code)
            || Utils.isNullOrEmpty(qty) || Utils.isNullOrEmpty(price)
            || Utils.isNullOrEmpty(unit) || Utils.isNullOrEmpty(discount_rate)
        ) {
            MessageBox().showLongToast(context, context.getString(R.string.require_fill_full_inf))
            return
        }

        loadingView.value = View.VISIBLE
        val c_name = RequestBody.create(MediaType.parse("text/plain"), name)
        val c_code = RequestBody.create(MediaType.parse("text/plain"), code)
        val c_qty = RequestBody.create(MediaType.parse("text/plain"), qty)
        val c_price = RequestBody.create(MediaType.parse("text/plain"), price)
        val c_discount_rate = RequestBody.create(MediaType.parse("text/plain"), discount_rate)
        val c_unit = RequestBody.create(MediaType.parse("text/plain"), unit)

        API.apiService.addProduct(
            c_name,
            c_code,
            c_qty,
            c_price,
            c_discount_rate,
            c_unit,
            createListFile(listPath)
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok),
                        object : DialogInterface.OnClickListener {
                            override fun onClick(p0: DialogInterface?, p1: Int) {
                                addSuccess.value = true
                                listFile.clear()
                            }
                        })
                } else {
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                }
            }, {
                loadingView.value = View.GONE
                MessageBox().showAlert(
                    context,
                    context.getString(R.string.err_internet),
                    context.getString(R.string.ok)
                )
                LogUtil.debug("__add_product__", it.toString())
            }).isDisposed
    }

    fun editProduct(
        name: String, code: String, qty: String, price: String, discount_rate: String,
        unit: String, listPath: List<String>
    ) {
        if (Utils.isNullOrEmpty(name) || Utils.isNullOrEmpty(code)
            || Utils.isNullOrEmpty(qty) || Utils.isNullOrEmpty(price)
            || Utils.isNullOrEmpty(unit) || Utils.isNullOrEmpty(discount_rate)
        ) {
            MessageBox().showLongToast(context, context.getString(R.string.require_fill_full_inf))
            return
        }

        loadingView.value = View.VISIBLE
        val c_id = RequestBody.create(
            MediaType.parse("text/plain"),
            currentProduct!!.product_id.toString()
        )
        val c_name = RequestBody.create(MediaType.parse("text/plain"), name)
        val c_code = RequestBody.create(MediaType.parse("text/plain"), code)
        val c_qty = RequestBody.create(MediaType.parse("text/plain"), qty)
        val c_price = RequestBody.create(MediaType.parse("text/plain"), price)
        val c_discount_rate = RequestBody.create(MediaType.parse("text/plain"), discount_rate)
        val c_unit = RequestBody.create(MediaType.parse("text/plain"), unit)

        API.apiService.editProduct(
            c_id,
            c_name,
            c_code,
            c_qty,
            c_price,
            c_discount_rate,
            c_unit,
            createListFile(listPath)
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok),
                        object : DialogInterface.OnClickListener {
                            override fun onClick(p0: DialogInterface?, p1: Int) {
                                editSuccess.value = true
                                listFile.clear()
                            }
                        })
                } else {
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                }
            }, {
                loadingView.value = View.GONE
                MessageBox().showAlert(
                    context,
                    context.getString(R.string.err_internet),
                    context.getString(R.string.ok)
                )
                LogUtil.debug("__edit_product__", it.toString())
            }).isDisposed
    }

    fun createListFile(listPath: List<String>): List<MultipartBody.Part> {
        for (i: Int in 0..listPath.size - 1) {
            if (convertToFilePath(listPath.get(i)) != null) {
                listFile.add(convertToFilePath(listPath.get(i))!!)
            }
        }
        return listFile
    }

    fun convertToFilePath(path: String): MultipartBody.Part? {
        return try {
            val uri = path
            var mediaType: String? = context.getContentResolver().getType(Uri.parse(uri))
            if (mediaType == null) {
                if (uri.endsWith("jpeg")) mediaType = "image/jpeg"
                if (uri.endsWith("jpg")) mediaType = "image/jpg"
                if (uri.endsWith("png")) mediaType = "image/png"
            }
            val file = File(path)
            val body: RequestBody =
                RequestBody.create(MediaType.parse(mediaType), file)
            MultipartBody.Part.createFormData("image[]", file.getName(), body)
        } catch (e: Throwable) {
            null
        }
    }
}