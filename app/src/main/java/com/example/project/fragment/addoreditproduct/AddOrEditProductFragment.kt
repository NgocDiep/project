package com.example.project.fragment.addoreditproduct

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.project.R
import com.example.project.base.BaseFragment
import com.example.project.constraint.Constraint
import com.example.project.constraint.Constraint.Companion.KEY_PRODUCT_DETAIL
import com.example.project.constraint.Constraint.Companion.KEY_SCAN_CODE
import com.example.project.constraint.Constraint.Companion.currentCode
import com.example.project.constraint.Constraint.Companion.currentProduct
import com.example.project.dialog.GenerateQRCodeDialog
import com.example.project.dialog.InputCodeDialog
import com.example.project.dialog.LDLMPhotoDialog
import com.example.project.model.Product
import com.example.project.navigation.MyNavigation
import com.example.project.utils.CheckPermission
import com.example.project.utils.FileImage
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import kotlinx.android.synthetic.main.fragment_add_product.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AddOrEditProductFragment : BaseFragment() {
    lateinit var inputCodeDialog: InputCodeDialog
    lateinit var generateQRCodeDialog: GenerateQRCodeDialog
    lateinit var photoDialog: LDLMPhotoDialog
    private val REQUEST_TAKE_PHOTO = 1
    private val REQUEST_PICK_PHOTO = 10
    var currentPhotoPath: String? = null
    private lateinit var photoAdapter: PhotoAdapter
    private lateinit var layoutManager: GridLayoutManager
    lateinit var viewModel: VMAddOrEditProductFragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_product, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dismissLoading()
        init()
        event()
        dataListener()
    }

    override fun onResume() {
        super.onResume()
        if (currentCode != "") {
            edt_code.setText(currentCode)
            item_code.expand()
        }

        if (KEY_PRODUCT_DETAIL == "add") {
            navbar.setTitle(context!!.getString(R.string.add_product))
        } else if (KEY_PRODUCT_DETAIL == "edit") {
            navbar.setTitle(context!!.getString(R.string.edit_product))
            fillData()
        }
    }

    fun init() {
        layoutManager = GridLayoutManager(context!!, 3)
        rv_image_product.layoutManager = layoutManager
        photoAdapter = PhotoAdapter(context!!)
        rv_image_product.adapter = photoAdapter

        viewModel = ViewModelProviders.of(activity!!).get(VMAddOrEditProductFragment::class.java)
        viewModel.init(context!!)
    }

    fun fillData() {
        if (currentProduct != null) {
            item_name.expand()
            edt_name.setText(currentProduct!!.product_name)
            item_code.expand()
            edt_code.setText(currentProduct!!.product_code)
            item_discount.expand()
            edt_discount_rate.setText((currentProduct!!.product_discount_rate))
            item_unit.expand()
            edt_unit.setText(currentProduct!!.unit)
            item_qty.expand()
            edt_qty.setText(currentProduct!!.product_qty)
            item_price.expand()
            edt_price.setText(currentProduct!!.product_price)
            activity.let {
                it!!.getWindow()
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            }
        } else {
            MessageBox().showLongToast(context, context!!.getString(R.string.err_try_again))
            activity?.let {
                it.onBackPressed()
            }
        }
    }

    fun event() {
        navbar.setBackPressListener {
            activity?.let {
                activity!!.onBackPressed()
            }
        }

        item_code.setOnClickListener {
            context?.let {
                inputCodeDialog = InputCodeDialog(it)
                inputCodeDialog.showDialog(scanCodeListener, generateCodeListener)
            }
        }

        btn_add_image.setOnClickListener {
            context?.let {
                if (currentProduct == null) {
                    currentProduct = Product(
                        -1, "", "", "",
                        "", "", "", "",
                        "", ArrayList(), ArrayList()
                    )
                }
                currentProduct!!.product_name = edt_name.text.toString()
                currentProduct!!.product_code = edt_code.text.toString()
                currentProduct!!.product_qty = edt_qty.text.toString()
                currentProduct!!.product_price = edt_price.text.toString()
                currentProduct!!.product_discount_rate = edt_discount_rate.text.toString()
                currentProduct!!.product_unit = edt_unit.text.toString()
                photoDialog = LDLMPhotoDialog(it)
                photoDialog.showDialog(onGetPhotoFromCamera, onGetPhotoFromLib)
            }
        }

        btn_add_product.setOnClickListener {
            if (KEY_PRODUCT_DETAIL == "add") {
                viewModel.addProduct(
                    edt_name.text.toString(), edt_code.text.toString(),
                    edt_qty.text.toString(), edt_price.text.toString(),
                    edt_discount_rate.text.toString(),
                    edt_unit.text.toString(), photoAdapter.listImagePath
                )
            } else if (KEY_PRODUCT_DETAIL == "edit") {
                viewModel.editProduct(
                    edt_name.text.toString(), edt_code.text.toString(),
                    edt_qty.text.toString(), edt_price.text.toString(),
                    edt_discount_rate.text.toString(),
                    edt_unit.text.toString(), photoAdapter.listImagePath
                )
            }
        }
    }

    private fun closePopupPhoto() {
        if (photoDialog != null && photoDialog.isShowing()) {
            photoDialog.dismiss()
        }
    }

    private val onGetPhotoFromCamera = View.OnClickListener {
        dispatchTakePictureIntent()
        closePopupPhoto()
    }

    private val onGetPhotoFromLib = View.OnClickListener {
        pickImageFromGallery()
        closePopupPhoto()
    }

    private fun pickImageFromGallery() {
        if (Build.VERSION.SDK_INT <= 19) {
            val i = Intent()
            i.type = "image/*"
            i.action = Intent.ACTION_GET_CONTENT
            i.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(
                i,
                REQUEST_PICK_PHOTO
            )
        } else if (Build.VERSION.SDK_INT > 19) {
            if (CheckPermission.checkPermissionREAD_EXTERNAL_STORAGE(activity!!)) {
                val i = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )
                startActivityForResult(
                    i,
                    REQUEST_PICK_PHOTO
                )
            } else {
                MessageBox().showLongToast(
                    context!!,
                    getString(R.string.permission_read_lib)
                )
            }
        }
    }

    private fun dispatchTakePictureIntent() {
        if (CheckPermission.checkPermissionCAMERA(this@AddOrEditProductFragment)) {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            var photoFile: File? = null

            try {
                val builder = StrictMode.VmPolicy.Builder()
                StrictMode.setVmPolicy(builder.build())
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (takePictureIntent.resolveActivity(activity!!.packageManager) != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile))
                startActivityForResult(
                    takePictureIntent,
                    REQUEST_TAKE_PHOTO
                )
            }
        }
    }


    @Throws(IOException::class)
    private fun createImageFile(): File? { // Create an image file name
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir: File? =
            activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        var image: File? = null
        if (storageDir != null) image = File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",  /* suffix */
            storageDir /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image!!.absolutePath
        return image
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_TAKE_PHOTO -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent()
                } else CheckPermission.showAlertPermissionCAMERA(activity!!)
            }
            REQUEST_PICK_PHOTO -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickImageFromGallery()
                } else CheckPermission.showAlertPermissionREAD_EXTERNAL_STORAGE(activity!!)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
                if (FileImage.checkFileImageNull(currentPhotoPath)) {
                    photoAdapter.append(currentPhotoPath!!)
                } else MessageBox().showLongToast(context!!, getString(R.string.err))
            }
            if (requestCode == REQUEST_PICK_PHOTO && resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val uri = data.data
                    // Get the file path from Uri
                    val path: String =
                        com.example.project.utils.FileUtils.getPath(context!!, uri)
                    if (path != null && com.example.project.utils.FileUtils.isLocal(path)) {
                        try {
                            currentPhotoPath = path
                            if (FileImage.checkFileImageNull(currentPhotoPath)) {
                                photoAdapter.append(currentPhotoPath!!)
                            } else MessageBox().showLongToast(
                                context!!,
                                getString(R.string.err)
                            )
                        } catch (e: Throwable) {
                            LogUtil.debug("__err_avatar__", e.toString())
                            MessageBox().showLongToast(
                                context!!,
                                getString(R.string.err)
                            )
                        }
                    }
                }
            }
            if (requestCode == KEY_SCAN_CODE && resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    edt_code.setText(data.getStringExtra(Constraint.KEY_CODE)!!)
                    item_code.expand()
                }
            }
        } catch (e: Throwable) {
            LogUtil.debug("__err_avatar__", e.toString())
            MessageBox().showLongToast(
                context!!,
                getString(R.string.err)
            )
        }
    }

    private val scanCodeListener = View.OnClickListener {
        activity?.let {
            MyNavigation.getInstance().goToZxingCodeScanner(it)
        }
        closePopup()
    }

    private val generateCodeListener = View.OnClickListener {
        activity?.let {
            generateQRCodeDialog = GenerateQRCodeDialog(it, {
                edt_code.setText(it)
                item_code.expand()
            })
            generateQRCodeDialog.show()
        }
        closePopup()
    }

    private fun closePopup() {
        if (inputCodeDialog != null && inputCodeDialog.isShowing()) {
            inputCodeDialog.dismiss()
        }
    }

    fun dataListener() {
        viewModel.loadingView.observe(activity!!, androidx.lifecycle.Observer {
            if (it == View.VISIBLE) {
                showLoading()
            } else dismissLoading()
        })

        viewModel.addSuccess.observe(activity!!, androidx.lifecycle.Observer {
            if (it) {
                photoAdapter.deleteAll()
                activity!!.onBackPressed()
            }
        })

        viewModel.editSuccess.observe(activity!!, androidx.lifecycle.Observer {
            if (it) {
                photoAdapter.deleteAll()
                MyNavigation.getInstance().backNumbFrag(2)
            }
        })
    }

    override fun onStop() {
        super.onStop()
        currentCode = ""
    }
}

