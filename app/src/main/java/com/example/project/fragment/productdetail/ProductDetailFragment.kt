package com.example.project.fragment.productdetail

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.project.R
import com.example.project.base.BaseFragment
import com.example.project.constraint.Constraint.Companion.KEY_PRODUCT_DETAIL
import com.example.project.constraint.Constraint.Companion.currentProduct
import com.example.project.constraint.Constraint.Companion.loadProductList
import com.example.project.constraint.Constraint.Companion.noCustomer
import com.example.project.constraint.Constraint.Companion.selectedTabResume
import com.example.project.dialog.AddToCartNoneCusDialog
import com.example.project.dialog.AskToAddCustomerDialog
import com.example.project.model.Product
import com.example.project.navigation.MyNavigation
import com.example.project.utils.LogUtil
import com.example.project.utils.formatMoney
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import kotlinx.android.synthetic.main.fragment_product_detail.*

class ProductDetailFragment : BaseFragment() {
    lateinit var dialog: AskToAddCustomerDialog
    lateinit var viewModel: VMProductDetailFragment
    lateinit var addToCartNoneCusDialog: AddToCartNoneCusDialog
    //do currentProduct đến khi bấm nút btn_edit bị null nên gán qua biến phụ
    private lateinit var thisProduct: Product

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(VMProductDetailFragment::class.java)
        viewModel.init(context!!)
    }

    override fun onResume() {
        super.onResume()
        fillData()
        event()
        dataListener()
    }

    fun fillData() {
        if (currentProduct != null) {
            thisProduct = currentProduct!!
            tv_name.text = currentProduct!!.product_name
            tv_price.text =
                context!!.getString(
                    R.string.price,
                    formatMoney(currentProduct!!.product_discount_price.toLong())
                )
            if (currentProduct!!.product_discount_rate != null &&
                !currentProduct!!.product_discount_rate.equals("0")
            ) {
                layout_discount.visibility = View.VISIBLE
                tv_discount.text =
                    context!!.getString(R.string.discount, currentProduct!!.product_discount_rate)
                tv_price_origin.visibility = View.VISIBLE
                tv_price_origin.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG)
                tv_price_origin.text =
                    context!!.getString(
                        R.string.price,
                        formatMoney(currentProduct!!.product_price.toLong())
                    )
            }
            tv_qty.text = context!!.getString(
                R.string.number_qty,
                currentProduct!!.product_qty,
                currentProduct!!.unit
            )
        }
        setSlider()
    }

    private fun setSlider() {
        val adapter = ProductImageAdapter(context!!)

        imageSlider?.apply {
            sliderAdapter = adapter
            setIndicatorAnimation(IndicatorAnimations.WORM)
            setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
            autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
            scrollTimeInSec = 10
        }
        adapter.refresh(currentProduct!!.medias)
    }

    fun event() {
        btn_edit.setOnClickListener {
            MyNavigation.getInstance().goToEditProduct()
            LogUtil.debug("__current_product__", currentProduct.toString())
            currentProduct = thisProduct
            KEY_PRODUCT_DETAIL = "edit"
        }

        btn_back.setOnClickListener {
            loadProductList = true
            MyNavigation.getInstance().backNumbFrag(1)
        }

        btn_add_to_cart.setOnClickListener {
            dialog = AskToAddCustomerDialog(context!!, {
                addToCartNoneCusDialog = AddToCartNoneCusDialog(
                    context!!, { qty, note ->
                        viewModel.addToCartNoneCustomer(
                            currentProduct!!.product_id.toString(),
                            qty,
                            note
                        )
                    }
                )
                addToCartNoneCusDialog.show()
                noCustomer = false
            }, {
                noCustomer = true
            })
            dialog.show()
        }
    }

    fun dataListener() {
        viewModel.loadingView.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                showLoading()
            } else dismissLoading()
        })

        viewModel.addSuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                selectedTabResume = "payment"
                MyNavigation.getInstance().backNumbFrag(1)
            }
        })
    }
}
