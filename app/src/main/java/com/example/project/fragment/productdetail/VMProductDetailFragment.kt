package com.example.project.fragment.productdetail

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.currentCart
import com.example.project.http.API
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class VMProductDetailFragment : ViewModel() {
    lateinit var context: Context
    lateinit var loadingView: MutableLiveData<Int>
    lateinit var addSuccess: MutableLiveData<Boolean>
    lateinit var body: HashMap<String, String>

    fun init(context: Context) {
        this.context = context
        loadingView = MutableLiveData()
        addSuccess = MutableLiveData()
    }

    fun addToCartNoneCustomer(product_id: String, product_qty: String, note: String) {
        loadingView.value = View.VISIBLE
        body = hashMapOf(
            "product_id" to product_id,
            "product_qty" to product_qty,
            "note" to note
        )
        API.apiService.addToCart(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    if (it.result != null) {
                        addSuccess.value = true
                        MessageBox().showLongToast(context, it.message)
                        currentCart = it.result!!
                    }
                } else {
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                }
            }, {
                loadingView.value = View.GONE
                MessageBox().showLongToast(context, context.getString(R.string.err_internet))
                LogUtil.debug("__add_to_cart_none_customer__", it.toString())
            }).isDisposed
    }
}