package com.example.project.fragment.configuration

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.currentConfig
import com.example.project.http.API
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class VMConfigurationFragment : ViewModel() {
    lateinit var context: Context
    lateinit var loadingView: MutableLiveData<Int>
    lateinit var body: HashMap<String, String>
    lateinit var getDiscountSuccess: MutableLiveData<Boolean>
    lateinit var createDiscountSuccess: MutableLiveData<Boolean>
    lateinit var getConfigVipSuccess: MutableLiveData<Boolean>
    lateinit var createConfigVipSuccess: MutableLiveData<Boolean>
    lateinit var getConfigSwitchPointSuccess: MutableLiveData<Boolean>
    lateinit var createConfigSwitchPointSuccess: MutableLiveData<Boolean>

    fun init(context: Context) {
        this.context = context
        loadingView = MutableLiveData(View.GONE)
        getDiscountSuccess = MutableLiveData()
        createDiscountSuccess = MutableLiveData()
        getConfigVipSuccess = MutableLiveData()
        createConfigVipSuccess = MutableLiveData()
        getConfigSwitchPointSuccess = MutableLiveData()
        createConfigSwitchPointSuccess = MutableLiveData()
    }

    fun getDiscountMember(type: String) {
        loadingView.value = View.VISIBLE
        body = hashMapOf(
            "type" to type
        )

        API.apiService.getDiscountMember(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    if (it.result != null) {
                        if (it.result!!.size > 0) {
                            currentConfig = it.result!!.get(0)
                        }
                        getDiscountSuccess.value = true
                    } else MessageBox().showAlert(
                        context,
                        "Bạn chưa cấu hình chiết khấu cho loại khách này",
                        context.getString(R.string.ok)
                    )
                } else MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
            }, {
                loadingView.value = View.GONE
                MessageBox().showLongToast(
                    context,
                    context.getString(R.string.err_internet)
                )
                LogUtil.debug("__err_get_discount_member__", it.toString())
            }).isDisposed
    }

    fun createDiscountMember(type: String, min_amount: String, discount_amount: String) {
        loadingView.value = View.VISIBLE

        body = hashMapOf(
            "type" to type,
            "min_amount" to min_amount,
            "discount_amount" to discount_amount
        )
//        LogUtil.debug("__err_create_discount__", "${type} - ${min_amount} - ${discount_amount}")

        API.apiService.createDiscountMember(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    createDiscountSuccess.value = true
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                } else {
                    MessageBox().showLongToast(context, it.message)
                }
            }, {
                LogUtil.debug("__err_create_discount__", it.toString())
                loadingView.value = View.GONE
                MessageBox().showLongToast(
                    context,
                    context.getString(R.string.err_internet)
                )
            }).isDisposed
    }

    fun getConfigVip() {
        loadingView.value = View.VISIBLE

        API.apiService.getConfigVip().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    if (it.result != null) {
                        currentConfig = it.result!!
                    }
                    getConfigVipSuccess.value = true
                } else MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
            }, {
                loadingView.value = View.GONE
                MessageBox().showLongToast(
                    context,
                    context.getString(R.string.err_internet)
                )
                LogUtil.debug("__err_get_config_vip__", it.toString())
            }).isDisposed
    }

    fun createConfigVip(amount: String) {
        loadingView.value = View.VISIBLE

        body = hashMapOf(
            "amount" to amount
        )

        API.apiService.createConfigVip(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    createConfigVipSuccess.value = true
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                } else {
                    MessageBox().showLongToast(context, it.message)
                }
            }, {
                LogUtil.debug("__err_create_config_vip__", it.toString())
                loadingView.value = View.GONE
                MessageBox().showLongToast(
                    context,
                    context.getString(R.string.err_internet)
                )
            }).isDisposed
    }

    fun getConfigSwitchPoint() {
        loadingView.value = View.VISIBLE

        API.apiService.getConfigSwitchPoint().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    if (it.result != null) {
                        currentConfig = it.result!!
                    }
                    getConfigSwitchPointSuccess.value = true
                } else MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
            }, {
                loadingView.value = View.GONE
                MessageBox().showLongToast(
                    context,
                    context.getString(R.string.err_internet)
                )
                LogUtil.debug("__err_get_config_switch_point__", it.toString())
            }).isDisposed
    }

    fun createConfigSwitchPoint(amount: String) {
        loadingView.value = View.VISIBLE

        body = hashMapOf(
            "amount" to amount
        )

        API.apiService.createConfigSwitchPoint(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    createConfigSwitchPointSuccess.value = true
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                } else MessageBox().showLongToast(context, it.message)
            }, {
                LogUtil.debug("__err_create_config_switch_point__", it.toString())
                loadingView.value = View.GONE
                MessageBox().showLongToast(
                    context,
                    context.getString(R.string.err_internet)
                )
            }).isDisposed
    }
}