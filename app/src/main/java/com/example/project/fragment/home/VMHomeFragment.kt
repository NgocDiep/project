package com.example.project.fragment.home

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project.R
import com.example.project.http.API
import com.example.project.model.Product
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class VMHomeFragment : ViewModel() {
    lateinit var context: Context
    lateinit var loadingView: MutableLiveData<Int>
    lateinit var result: MutableLiveData<Product>

    fun init(context: Context) {
        this.context = context
        loadingView = MutableLiveData()
        result = MutableLiveData()
    }

    fun searchProductByCode(code: String) {
        loadingView.value = View.VISIBLE
        val body: HashMap<String, String> = hashMapOf(
            "page" to "1",
            "product_name" to "",
            "code" to code
        )
        API.apiService.getListProduct(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    if (it.result != null) {
                        if (it.result!!.size > 0) {
                            result.value = it.result!!.get(0)
                            LogUtil.debug("__not_found_product__", "TIM THAY SAN PHAM")
                        } else if (it.result!!.size == 0) {
                            LogUtil.debug("__not_found_product__", "KHONG TIM THAY SAN PHAM")
                            MessageBox().showLongToast(context, "Không tìm thấy sản phẩm")
                        }
                    }
                } else {
                    LogUtil.debug("__not_found_product__", "KHONG TIM THAY SAN PHAM")
                    MessageBox().showLongToast(context, it.message)
                }
            }, {
                LogUtil.debug("__err_search_product_by_code__", it.toString())
                loadingView.value = View.GONE
                MessageBox().showLongToast(context, context.getString(R.string.err_internet))
            }).isDisposed
    }
}