package com.example.project.fragment.customer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.project.R
import com.example.project.base.BaseFragment
import com.example.project.constraint.Constraint.Companion.currentCustomer
import com.example.project.dialog.AddCustomerDialog
import com.example.project.navigation.MyNavigation
import com.example.project.utils.MessageBox
import com.example.project.utils.RecyclerItemCLickListener
import com.example.project.utils.setOnLoadMore
import kotlinx.android.synthetic.main.fragment_customer.*

class CustomerFragment : BaseFragment() {
    lateinit var viewModel: VMCustomerFragment
    lateinit var customerAdapter: CustomerAdapter
    lateinit var layoutManager: RecyclerView.LayoutManager
    private var pageIndex = 1
    private var customer_name = ""
    private var customer_phone = ""
    lateinit var addCustomerDialog: AddCustomerDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_customer, container, false)
    }

    override fun onResume() {
        super.onResume()
        loadAll()
    }

    fun loadAll() {
        pageIndex = 1
        init()
        event()
        dataListener()
        fetchData()
    }

    fun init() {
        viewModel = ViewModelProviders.of(this@CustomerFragment).get(VMCustomerFragment::class.java)
        viewModel.init(context!!)

        customerAdapter = CustomerAdapter(context!!)
        layoutManager = GridLayoutManager(context!!, 2)
        rv_customer.adapter = customerAdapter
        rv_customer.layoutManager = layoutManager
    }

    fun fetchData() {
        viewModel.getListCustomer(pageIndex.toString(), customer_name, customer_phone)
    }

    fun event() {
        iv_setting.setOnClickListener {
            MyNavigation.getInstance().goToConfiguration()
        }

        rl_search.setOnClickListener {
            viewflipper_input.displayedChild = 1
            viewflipper_search.displayedChild = 1
            btn_search.setOnClickListener {
                if (edt_name.text.toString() == "" && edt_phone.text.toString() == "") {
                    MessageBox().showLongToast(
                        context!!,
                        context!!.getString(R.string.require_input_key)
                    )
                } else {
                    customer_name = edt_name.text.toString()
                    customer_phone = edt_phone.text.toString()
                    loadAll()
                }
            }
            btn_clear_filter.setOnClickListener {
                customer_name = ""
                customer_phone = ""
                edt_name.setText("")
                edt_phone.setText("")
                loadAll()
                viewflipper_search.displayedChild = 0
                viewflipper_input.displayedChild = 0
            }
        }

        btn_add_customer.setOnClickListener {
            addCustomerDialog = AddCustomerDialog(
                context!!,
                { customer_name, customer_phone, customer_point ->
                    viewModel.addCustomer(customer_name, customer_phone)
                })
            addCustomerDialog.show()
        }

        rv_customer.addOnItemTouchListener(
            RecyclerItemCLickListener(
                context!!,
                rv_customer,
                object :
                    RecyclerItemCLickListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        currentCustomer = viewModel.listCustomer.value!![position]
                        addCustomerDialog = AddCustomerDialog(
                            context!!,
                            { customer_name, customer_phone, customer_point ->
                                viewModel.editCustomer(
                                    customer_name,
                                    customer_phone,
                                    customer_point!!
                                )
                            })
                        addCustomerDialog.show()
                    }

                    override fun onLongClick(view: View?, position: Int) {
                    }
                })
        )

        rv_customer.setOnLoadMore {
            if (viewModel.listCustomer.value!!.size >= 10) {
                pageIndex++
                viewModel.getListCustomer(pageIndex.toString(), customer_name, customer_phone)
            }
        }
    }

    fun dataListener() {
        viewModel.loadingView.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                showLoading()
            } else dismissLoading()
        })

        viewModel.listCustomer.observe(viewLifecycleOwner, Observer {
            if (it.size > 0) {
                viewflipper.displayedChild = 0
                customerAdapter.refresh(it)
            } else {
                viewflipper.displayedChild = 1
            }
        })

        viewModel.addCustomerSuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                addCustomerDialog.cancel()
                currentCustomer = null
                loadAll()
            }
        })
    }
}
