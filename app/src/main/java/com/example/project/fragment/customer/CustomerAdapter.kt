package com.example.project.fragment.customer

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.project.R
import com.example.project.model.Customer
import com.example.project.utils.formatMoney
import kotlinx.android.synthetic.main.layout_item_customer.view.*
import kotlinx.android.synthetic.main.layout_item_product.view.tv_name

class CustomerAdapter(val context: Context) :
    RecyclerView.Adapter<CustomerAdapter.CustomerViewHolder>() {
    val listProduct = ArrayList<Customer>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.layout_item_customer, parent, false)
        return CustomerViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listProduct.size
    }

    override fun onBindViewHolder(holder: CustomerViewHolder, position: Int) {
        holder.bindView(listProduct.get(position))
    }

    fun append(list: ArrayList<Customer>) {
        this.listProduct.addAll(list)
        this.notifyDataSetChanged()
    }

    fun append(customer: Customer) {
        this.listProduct.add(customer)
        this.notifyDataSetChanged()
    }

    fun refresh(list: ArrayList<Customer>) {
        this.listProduct.clear()
        this.listProduct.addAll(list)
        this.notifyDataSetChanged()
    }

    inner class CustomerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(item: Customer) {
            itemView.tv_name.text = item.customer_name
            itemView.tv_phone.text = context.getString(R.string.customer_phone, item.customer_phone)
            itemView.tv_point.text = item.customer_point
            if (item.customer_type == "0") {
                itemView.tv_type.text = "Thường"
            } else {
                itemView.tv_type.text = "VIP"
                itemView.ll_item.setBackgroundColor(context.resources.getColor(R.color.color_app_10))
            }
            itemView.tv_total.text = "Chi tiêu: ${context.getString(
                R.string.price,
                formatMoney(item.customer_total_amount.toLong())
            )}"
        }
    }
}