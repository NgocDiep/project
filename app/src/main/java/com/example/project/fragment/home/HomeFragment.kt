package com.example.project.fragment.home

import android.graphics.Color
import android.os.Bundle
import android.os.Process
import android.view.*
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.project.R
import com.example.project.base.BaseFragment
import com.example.project.constraint.Constraint.Companion.currentCode
import com.example.project.constraint.Constraint.Companion.currentProduct
import com.example.project.constraint.Constraint.Companion.loadProductList
import com.example.project.constraint.Constraint.Companion.selectedTabResume
import com.example.project.dialog.ExitAppDialog
import com.example.project.fragment.customer.CustomerFragment
import com.example.project.fragment.payment.PaymentFragment
import com.example.project.fragment.product.ProductFragment
import com.example.project.fragment.revuene.RevueneFragment
import com.example.project.navigation.MyNavigation
import com.example.project.utils.MessageBox
import com.example.project.view.TabBarItem
import kotlinx.android.synthetic.main.fragment_home.*
import kotlin.system.exitProcess


class HomeFragment : BaseFragment() {
    private lateinit var tabBarItems: List<TabBarItem>
    private var currentIndex = 0
    var onBack: MutableLiveData<Boolean> = MutableLiveData(false)
    var showed: Boolean = false
    lateinit var viewModel: VMHomeFragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(VMHomeFragment::class.java)
        viewModel.init(context!!)
    }

    override fun onResume() {
        super.onResume()
        onBackPressed()
        setTabbar()
        event()
        dataListener()

        if (currentCode != "") {
            viewModel.searchProductByCode(currentCode)
        }
//
//        if (selectedTabResume == "payment") {
//            selectTab(1)
//            selectedTabResume = ""
//        } else if (selectedTabResume == "customer") {
//            selectTab(3)
//            selectedTabResume = ""
////        } else if (selectedTabResume == "product") {
////            selectTab(0)
////            selectedTabResume = ""
//        } else if (selectedTabResume == "revuene") {
//            selectTab(2)
//            selectedTabResume = ""
//        }
    }

    private fun setTabbar() {
        val navigationBarColor = activity!!.window.navigationBarColor
        activity!!.window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        activity!!.window.statusBarColor = Color.TRANSPARENT
        activity!!.window.navigationBarColor = Color.TRANSPARENT
        activity!!.window.navigationBarColor = navigationBarColor

        val fragments = listOf(
            ProductFragment(),
            PaymentFragment(),
            RevueneFragment(),
            CustomerFragment()
        )
        vpHome.adapter = HomePagerAdapter(childFragmentManager, fragments)
        vpHome.offscreenPageLimit = 4

        tabBarItems = listOf(
            item_product,
            item_payment,
            item_revuene,
            item_discount
        )

        tabBarItems.forEachIndexed { index, tabBarItem ->
            tabBarItem.setOnClickListener {
                selectTab(index)
            }
        }

        if (selectedTabResume == "payment") {
            selectTab(1)
            selectedTabResume = ""
        } else if (selectedTabResume == "customer") {
            selectTab(3)
            selectedTabResume = ""
        } else if (selectedTabResume == "product" || selectedTabResume == "") {
//            loadProductList = true
            selectTab(0)
            selectedTabResume = ""
        } else if (selectedTabResume == "revuene") {
            selectTab(2)
            selectedTabResume = ""
        }
        if (selectedTabResume == "") {
            loadProductList = true
        }
    }

    private fun selectTab(index: Int) {
        this.currentIndex = index
        tabBarItems.forEachIndexed { i, tabBarItem ->
            tabBarItem.setActive(i == index)
        }
        vpHome.setCurrentItem(index, false)
    }

    private fun event() {
        item_scanner.setOnClickListener {
            activity?.let {
                MyNavigation.getInstance().goToZxingCodeScannerToSearch(it)
            }
        }

        fab.setOnClickListener {
            MyNavigation.getInstance().goToAccount()
        }
    }

    fun onBackPressed() {
        activity?.let {
            view!!.setFocusableInTouchMode(true)
            view!!.requestFocus()
            view!!.setOnKeyListener(object : View.OnKeyListener {
                override fun onKey(p0: View?, p1: Int, p2: KeyEvent?): Boolean {
                    if (p1 == KeyEvent.KEYCODE_BACK) {
                        onBack.value = true
                        return true
                    }
                    return false
                }
            })
        }
    }

    fun dataListener() {
        onBack.observe(viewLifecycleOwner, Observer {
            if (it && !showed) {
                activity?.let {
                    ExitAppDialog(it, {
                        showed = false
                        onBack.value = false
                    }, {
                        it.moveTaskToBack(true)
                        Process.killProcess(Process.myPid())
                        exitProcess(1)
                    }).show()
                    showed = true
                }
            }
        })

        viewModel.loadingView.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                showLoading()
            } else {
                dismissLoading()
            }
        })

        viewModel.result.observe(viewLifecycleOwner, Observer {
            currentCode = ""
            if (it != null) {
                dismissLoading()
                currentProduct = it
                MyNavigation.getInstance().goToProductDetail()
            } else MessageBox().showLongToast(context!!, context!!.getString(R.string.err))
        })
    }
}
