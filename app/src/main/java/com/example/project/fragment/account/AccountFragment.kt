package com.example.project.fragment.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.project.R
import com.example.project.datalocal.Preferences
import com.example.project.navigation.MyNavigation
import kotlinx.android.synthetic.main.fragment_account.*

class AccountFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fillData()
        event()
    }

    fun fillData() {
        if (Preferences.getInstance().getUsername() != null) {
            tv_username.text = Preferences.getInstance().getUsername()
        }
        if (Preferences.getInstance().getAvatar() != null) {
            Glide.with(context!!)
                .load("http://hrm.gtechvn.org/storage/upload/" + Preferences.getInstance().getAvatar())
                .error(R.drawable.ic_user)
                .placeholder(R.drawable.ic_user).into(iv_avatar)
        }
        if (Preferences.getInstance().getFullname() != null) {
            tv_fullname.text = Preferences.getInstance().getFullname()
        }
        if (Preferences.getInstance().getPhone() != null) {
            tv_phone.text = Preferences.getInstance().getPhone()
        }
        if (Preferences.getInstance().getEmail() != null) {
            tv_email.text = Preferences.getInstance().getEmail()
        }
        if (Preferences.getInstance().getAddress() != null) {
            tv_address.text = Preferences.getInstance().getAddress()
        }
    }

    fun event() {
        navbar.setBackPressListener {
            MyNavigation.getInstance().backNumbFrag(1)
        }

        btn_logout.setOnClickListener {
            Preferences.getInstance().clearAccount()
            MyNavigation.getInstance().goToLogin(activity!!)
        }
    }
}
