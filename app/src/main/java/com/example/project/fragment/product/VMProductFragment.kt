package com.example.project.fragment.product

import android.content.Context
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project.R
import com.example.project.http.API
import com.example.project.model.Product
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class VMProductFragment : ViewModel() {
    lateinit var context: Context
    lateinit var loadingView: MutableLiveData<Int>
    lateinit var body: HashMap<String, String>
    lateinit var listProduct: MutableLiveData<ArrayList<Product>>
    var tmpData = ArrayList<Product>()
    var totalPage: Int = 1

    fun init(context: Context) {
        this.context = context
        this.loadingView = MutableLiveData(View.GONE)
        this.listProduct = MutableLiveData()
    }

    fun getListProduct(page: String, product_name: String, code: String) {
        loadingView.value = View.VISIBLE
        body = hashMapOf(
            "page" to page,
            "product_name" to product_name,
            "code" to code
        )
        API.apiService.getListProduct(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    totalPage = it.total_page
                    if (it.result != null) {
                        if (page.toInt() == 1) {
                            tmpData.clear()
                        }
                        tmpData.addAll(it.result!!)
                        listProduct.value = tmpData
                    }
                } else {
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                }
            }, {
                loadingView.value = View.GONE
                listProduct.value = ArrayList()
                MessageBox().showAlert(
                    context,
                    context.getString(R.string.err_internet),
                    context.getString(R.string.ok)
                )
                LogUtil.debug("__err_get_list_product__", it.toString())
            }).isDisposed
    }
}