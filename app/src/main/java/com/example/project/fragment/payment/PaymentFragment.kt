package com.example.project.fragment.payment

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.project.R
import com.example.project.base.BaseFragment
import com.example.project.constraint.Constraint.Companion.cartNoCustomer
import com.example.project.constraint.Constraint.Companion.codeProductAddToCart
import com.example.project.constraint.Constraint.Companion.currentCart
import com.example.project.constraint.Constraint.Companion.currentCustomer
import com.example.project.constraint.Constraint.Companion.currentProduct
import com.example.project.constraint.Constraint.Companion.selectedTabResume
import com.example.project.dialog.*
import com.example.project.model.Customer
import com.example.project.navigation.MyNavigation
import com.example.project.utils.MessageBox
import com.example.project.utils.Utils
import com.example.project.utils.formatMoney
import kotlinx.android.synthetic.main.fragment_payment.*

/**
 * A simple [Fragment] subclass.
 */
class PaymentFragment : BaseFragment() {
    lateinit var viewModel: VMPaymentFragment
    lateinit var adapter: PaymentAdapter
    lateinit var layoutManager: RecyclerView.LayoutManager
    lateinit var customerPaymentDialog: CustomerPaymentDialog
    lateinit var searchProductDialog: SearchProductDialog
    lateinit var addToCartDialog: AddToCartDialog
    var customer_id = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(VMPaymentFragment::class.java)
        viewModel.init(context!!)
    }

    override fun onResume() {
        super.onResume()
        setAdapter()
        fillData()
        event()
        dataListener()

        if (codeProductAddToCart != "") {
            viewModel.getProduct(codeProductAddToCart)
        }
    }

    fun setAdapter() {
        adapter = PaymentAdapter(context!!)
        layoutManager = LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
        rv_quote_item.adapter = adapter
        rv_quote_item.layoutManager = layoutManager
    }

    fun event() {
        btn_add.setOnClickListener {
            if (viewModel.currentCustomer.value != null) {
                if (viewModel.currentCustomer.value!!.customer_id != null) {
                    customer_id = viewModel.currentCustomer.value!!.customer_id.toString()
                }
            }
            searchProductDialog = SearchProductDialog(context!!, {
                viewModel.getProduct(it)
            }, {
                selectedTabResume = "payment"
                MyNavigation.getInstance().goToZxingCodeScannerToPay(activity!!)
            })
            searchProductDialog.show()
        }

        btn_pay.setOnClickListener {
            var confirmPayDialog = ConfirmPayDialog(context!!, {
                if (currentCustomer == null) {
                    viewModel.pay(currentCart!!.id.toString(), "-1")
                } else {
                    if (edt_point_payment.text.toString() != "") {
                        var point = edt_point_payment.text.toString().toInt()
                        if (point > currentCart!!.customer_point) {
                            MessageBox().showLongToast(
                                context,
                                "Khách hàng chỉ có ${currentCart!!.customer_point} điểm"
                            )
                        } else
                            viewModel.pay(
                                currentCart!!.id.toString(),
                                edt_point_payment.text.toString()
                            )
                    } else
                        viewModel.pay(
                            currentCart!!.id.toString(), "0"
                        )
                }
            })
            confirmPayDialog.show()
        }

        adapter.setOnDeleteItemCLick {
            viewModel.deleteQuoteItem(it.toString(), customer_id)
        }
    }

    fun fillData() {
        if (currentCart != null) {
            viewflipper.displayedChild = 0
            if (Utils.isNullOrEmpty(currentCart!!.customer_name) || Utils.isNullOrEmpty(
                    currentCart!!.customer_phone
                )
            ) {
                ll_customer.visibility = View.GONE
                ll_input_point.visibility = View.GONE
            } else {
                ll_customer.visibility = View.VISIBLE
                tv_customer_name.text = currentCart!!.customer_name
                tv_customer_phone.text =
                    context!!.getString(R.string.customer_phone, currentCart!!.customer_phone)
                tv_customer_point.text =
                    context!!.getString(
                        R.string.customer_point,
                        currentCart!!.customer_point.toString()
                    )
                ll_input_point.visibility = View.VISIBLE
            }

            adapter.refresh(currentCart!!.quote_item)

            if (currentCart!!.discount_amount != null) {
                if (currentCart!!.discount_amount != "0") {
                    tv_total_amount.visibility = View.VISIBLE
                    tv_total_amount.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG)
                    tv_total_amount.text = context!!.getString(
                        R.string.price,
                        formatMoney(currentCart!!.total_amount)
                    )
                }
            }

            tv_total_amount_receive.text =
                context!!.getString(
                    R.string.price,
                    formatMoney(currentCart!!.total_amount_receive)
                )
        } else {
            if (cartNoCustomer) {
                viewflipper.displayedChild = 0
                ll_customer.visibility = View.GONE
                ll_input_point.visibility = View.GONE
            } else {
                viewflipper.displayedChild = 1
                btn_add_new.setOnClickListener {
                    var askToAddCustomerDialog = AskToAddCustomerDialog(context!!, {
                        viewflipper.displayedChild = 0
                        ll_customer.visibility = View.GONE
                        ll_input_point.visibility = View.GONE
                        cartNoCustomer = true
                    }, {
                        customerPaymentDialog = CustomerPaymentDialog(context!!, {
                            viewModel.getCustomer(it)
                        }, { customer_name, customer_phone ->
                            viewModel.addCustomer(customer_name, customer_phone)
                        })
                        customerPaymentDialog.show()
                    })
                    askToAddCustomerDialog.show()
                }
            }
        }

        if (currentCustomer != null) {
            viewflipper.displayedChild = 0
            fillCustomer(currentCustomer!!)
        }
    }

    fun fillCustomer(customer: Customer) {
        viewflipper.displayedChild = 0
        ll_customer.visibility = View.VISIBLE
        tv_customer_name.text = customer.customer_name
        tv_customer_phone.text =
            context!!.getString(R.string.customer_phone, customer.customer_phone)
        tv_customer_point.text =
            context!!.getString(R.string.customer_point, customer.customer_point)
    }

    fun dataListener() {
        viewModel.loadingView.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                showLoading()
            } else dismissLoading()
        })

        viewModel.paySuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                currentCart = null
                currentCustomer = null
                currentProduct = null
                codeProductAddToCart = ""
                cartNoCustomer = false
                tv_total_amount.visibility = View.GONE
                tv_total_amount_receive.setText("đ0")
                tv_customer_point.setText("")
                viewModel.currentProduct = MutableLiveData()
                viewflipper.displayedChild = 1
            }
        })

        viewModel.newCart.observe(viewLifecycleOwner, Observer {
            fillData()
            event()
        })

        viewModel.currentCustomer.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                customerPaymentDialog.cancel()
                currentCustomer = it
                fillCustomer(it)
            }
        })

        viewModel.currentProduct.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                currentProduct = it
                searchProductDialog.cancel()
                addToCartDialog = AddToCartDialog(context!!, {
                    viewModel.addToCart(
                        customer_id,
                        viewModel.currentProduct.value!!.product_id.toString(),
                        it,
                        ""
                    )
                })
                addToCartDialog.show()
            }
        })
    }

    override fun onStop() {
        super.onStop()
        currentProduct = null
    }
}
