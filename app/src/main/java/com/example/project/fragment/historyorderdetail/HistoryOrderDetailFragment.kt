package com.example.project.fragment.historyorderdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.currentHistoryOrder
import com.example.project.constraint.Constraint.Companion.removeDeleteIcon
import com.example.project.constraint.Constraint.Companion.selectedTabResume
import com.example.project.fragment.payment.PaymentAdapter
import com.example.project.navigation.MyNavigation
import com.example.project.utils.formatDate
import com.example.project.utils.formatMoney
import kotlinx.android.synthetic.main.fragment_history_order_detail.*

class HistoryOrderDetailFragment : Fragment() {
    lateinit var quoteItemAdapter: PaymentAdapter
    lateinit var layoutManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history_order_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fillData()
        event()
    }

    fun fillData() {
        currentHistoryOrder?.let {
            if (it.customer_name != null && it.customer_phone != null) {
                viewflipper.displayedChild = 0
                tv_name.text = it.customer_name
                tv_phone.text = context!!.getString(R.string.customer_phone, it.customer_phone)
            } else viewflipper.displayedChild = 1

            tv_date_order.text =
                context!!.getString(R.string.order_date, formatDate(it.created_at, "dd/MM/yyyy"))

            quoteItemAdapter = PaymentAdapter(context!!)
            layoutManager = LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
            rv_quote_item.layoutManager = layoutManager
            rv_quote_item.adapter = quoteItemAdapter
            removeDeleteIcon = true
            quoteItemAdapter.refresh(it.item)

            if (it.total_amount == it.total_amount_receive) {
                ll_discount.visibility = View.GONE
                ll_point_used.visibility = View.GONE
                ll_total_amount.visibility = View.GONE
            }

            tv_total_amount.text =
                context!!.getString(R.string.price, formatMoney(it.total_amount.toLong()))
            tv_total_amount_receive.text =
                context!!.getString(R.string.price, formatMoney(it.total_amount_receive.toLong()))
            if (it.discount_amount == null) {
                ll_discount.visibility = View.GONE
            } else {
                if (it.discount_amount.toLong() != 0L) {
                    tv_discount.text =
                        context!!.getString(
                            R.string.price,
                            formatMoney(it.discount_amount.toLong())
                        )
                }
            }
            if (it.point_used.toInt() == 0) {
                ll_point_used.visibility = View.GONE
            } else tv_point_used.text = it.point_used
        }
    }

    fun event() {
        navbar.setBackPressListener {
            selectedTabResume = "revuene"
            MyNavigation.getInstance().backNumbFrag(1)
        }
    }

    override fun onStop() {
        super.onStop()
        removeDeleteIcon = false
    }
}
