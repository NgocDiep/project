package com.example.project.fragment.configuration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.project.R
import com.example.project.base.BaseFragment
import com.example.project.constraint.Constraint.Companion.currentDiscountType
import com.example.project.constraint.Constraint.Companion.currentType
import com.example.project.constraint.Constraint.Companion.selectedTabResume
import com.example.project.dialog.ConfigVipDialog
import com.example.project.dialog.DiscountDialog
import com.example.project.navigation.MyNavigation
import kotlinx.android.synthetic.main.fragment_configuration.*

class ConfigurationFragment : BaseFragment() {
    lateinit var viewModel: VMConfigurationFragment
    lateinit var discountDialog: DiscountDialog
    lateinit var configVipDialog: ConfigVipDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_configuration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        event()
        dataListener()
    }

    fun init() {
        viewModel = ViewModelProviders.of(this@ConfigurationFragment)
            .get(VMConfigurationFragment::class.java)
        viewModel.init(context!!)
    }

    fun event() {
        navbar.setBackPressListener {
            selectedTabResume = "customer"
            MyNavigation.getInstance().backNumbFrag(1)
        }

        tv_discount.setOnClickListener {
            ll_discount_detail.visibility = View.VISIBLE
            ll_formal_customer.setOnClickListener {
                currentDiscountType = "normal"
                viewModel.getDiscountMember("0")
            }
            ll_vip_customer.setOnClickListener {
                currentDiscountType = "vip"
                viewModel.getDiscountMember("1")
            }
        }

        tv_amount_for_vip.setOnClickListener {
            currentType = "config_vip"
            viewModel.getConfigVip()
        }

        tv_config_point_switch.setOnClickListener {
            currentType = "config_switch_point"
            viewModel.getConfigSwitchPoint()
        }
    }

    fun dataListener() {
        viewModel.loadingView.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                showLoading()
            } else dismissLoading()
        })

        viewModel.getDiscountSuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                discountDialog = DiscountDialog(context!!, { type, min_amount, discount_amount ->
                    viewModel.createDiscountMember(type, min_amount, discount_amount)
                })
                discountDialog.show()
            }
        })

        viewModel.createDiscountSuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                discountDialog.cancel()
            }
        })

        viewModel.getConfigVipSuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                configVipDialog = ConfigVipDialog(context!!, {
                    viewModel.createConfigVip(it)
                })
                configVipDialog.show()
            }
        })

        viewModel.createConfigVipSuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                configVipDialog.cancel()
            }
        })

        viewModel.getConfigSwitchPointSuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                configVipDialog = ConfigVipDialog(context!!, {
                    viewModel.createConfigSwitchPoint(it)
                })
                configVipDialog.show()
            }
        })

        viewModel.createConfigSwitchPointSuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                configVipDialog.cancel()
            }
        })
    }
}
