package com.example.project.fragment.product

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.project.R
import com.example.project.model.Product
import com.example.project.utils.formatMoney
import kotlinx.android.synthetic.main.layout_item_product.view.*

class ProductAdapter(val context: Context) :
    RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {
    val listProduct = ArrayList<Product>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_item_product, parent, false)
        return ProductViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listProduct.size
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bindView(listProduct.get(position))
    }

    fun append(list: ArrayList<Product>) {
        this.listProduct.addAll(list)
        this.notifyDataSetChanged()
    }

    fun append(product: Product) {
        this.listProduct.add(product)
        this.notifyDataSetChanged()
    }

    fun refresh(list: ArrayList<Product>) {
        this.listProduct.clear()
        this.listProduct.addAll(list)
        this.notifyDataSetChanged()
    }

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("ResourceAsColor")
        fun bindView(item: Product) {
            if (item.product_qty.toInt() == 0) {
                itemView.rl_out_of_product.visibility = View.VISIBLE
                itemView.tv_qty.visibility = View.GONE
                itemView.tv_price.setTextColor(R.color.color_grey_1)
            } else if (item.product_qty.toInt() > 0) {
                itemView.rl_out_of_product.visibility = View.GONE
                itemView.tv_qty.visibility = View.VISIBLE
                itemView.tv_qty.text =
                    context.getString(R.string.number_qty, item.product_qty, item.unit)
            }
            itemView.tv_name.text = item.product_name
            itemView.tv_price.text =
                context.getString(R.string.price, formatMoney(item.product_discount_price.toLong()))
            if (item.product_discount_rate != null && !item.product_discount_rate.equals("0")) {
                itemView.layout_discount.visibility = View.VISIBLE
                itemView.tv_discount.text =
                    context.getString(R.string.discount, item.product_discount_rate)
                itemView.tv_price_origin.visibility = View.VISIBLE
                itemView.tv_price_origin.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG)
                itemView.tv_price_origin.text =
                    context.getString(R.string.price, formatMoney(item.product_price.toLong()))
            } else {
                itemView.layout_discount.visibility = View.GONE
                itemView.tv_price_origin.visibility = View.GONE
            }
            Glide.with(context).load(item.medias.get(0))
                .placeholder(R.drawable.ic_product_active)
                .error(R.drawable.ic_product_active).into(itemView.iv_image)
        }
    }
}