package com.example.project.fragment.payment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.removeDeleteIcon
import com.example.project.model.QuoteItem
import com.example.project.utils.formatMoney
import kotlinx.android.synthetic.main.layout_item_quote_item.view.*

class PaymentAdapter(val context: Context) :
    RecyclerView.Adapter<PaymentAdapter.PaymentViewHolder>() {
    private var listQuoteItem = ArrayList<QuoteItem>()
    private lateinit var onDeleteItemCLick: ((Int) -> Unit)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.layout_item_quote_item, parent, false)
        return PaymentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listQuoteItem.size
    }

    override fun onBindViewHolder(holder: PaymentViewHolder, position: Int) {
        holder.bindView(listQuoteItem.get(position))
    }

    fun refresh(list: List<QuoteItem>) {
        listQuoteItem.clear()
        listQuoteItem.addAll(list)
        this.notifyDataSetChanged()
    }

    fun deleteItem(id: Int) {
        for (i in 0..listQuoteItem.size) {
            if (listQuoteItem.get(i).product_id == id) {
                listQuoteItem.removeAt(i)
            }
        }
        this.notifyDataSetChanged()
    }

    fun setOnDeleteItemCLick(onDeleteItemCLick: ((Int) -> Unit)) {
        this.onDeleteItemCLick = onDeleteItemCLick
    }

    inner class PaymentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(item: QuoteItem) {
            if (removeDeleteIcon) {
                itemView.ll_delete.visibility = View.GONE
            }
            itemView.tv_name.text = item.product_name
            itemView.tv_quote_price.text =
                context.getString(R.string.price, formatMoney(item.price.toLong()))
            itemView.tv_quote_qty.text = context.getString(R.string.qty_, item.qty)

            itemView.ll_delete.setOnClickListener {
                onDeleteItemCLick.invoke(item.id)
            }
        }
    }
}