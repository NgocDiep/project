package com.example.project.fragment.productdetail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.project.R;
import com.example.project.activity.welcome.SliderAdapter;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class ProductImageAdapter extends
        SliderViewAdapter<ProductImageAdapter.ProductImageViewHolder> {

    private Context context;
    private List<String> listPath = new ArrayList<>();

    public ProductImageAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ProductImageViewHolder onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_product_image, null);
        return new ProductImageViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(ProductImageViewHolder viewHolder, final int position) {
        Glide.with(viewHolder.itemView)
                .load(listPath.get(position))
                .error(R.drawable.ic_product_active)
                .placeholder(R.drawable.ic_product_active)
                .into(viewHolder.imageView);
    }

    @Override
    public int getCount() {
        return listPath.size();
    }

    public void refresh(List<String> sliderItems) {
        this.listPath = sliderItems;
        notifyDataSetChanged();
    }

    static class ProductImageViewHolder extends SliderViewAdapter.ViewHolder {
        View itemView;
        ImageView imageView;

        public ProductImageViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            imageView = itemView.findViewById(R.id.iv_image);
        }
    }

}
