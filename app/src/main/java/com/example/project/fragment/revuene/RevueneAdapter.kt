package com.example.project.fragment.revuene

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.project.R
import com.example.project.model.HistoryOrder
import com.example.project.utils.formatMoney
import kotlinx.android.synthetic.main.layout_item_history_order.view.*

class RevueneAdapter(val context: Context) :
    RecyclerView.Adapter<RevueneAdapter.HistoryOrderViewHolder>() {
    val listHistoryOrder = ArrayList<HistoryOrder>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryOrderViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.layout_item_history_order, parent, false)
        return HistoryOrderViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listHistoryOrder.size
    }

    override fun onBindViewHolder(holder: HistoryOrderViewHolder, position: Int) {
        holder.bindView(listHistoryOrder.get(position))
    }

    fun append(list: ArrayList<HistoryOrder>) {
        this.listHistoryOrder.addAll(list)
        this.notifyDataSetChanged()
    }

    fun append(historyOrder: HistoryOrder) {
        this.listHistoryOrder.add(historyOrder)
        this.notifyDataSetChanged()
    }

    fun refresh(list: ArrayList<HistoryOrder>) {
        this.listHistoryOrder.clear()
        this.listHistoryOrder.addAll(list)
        this.notifyDataSetChanged()
    }

    inner class HistoryOrderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(item: HistoryOrder) {
            if (item.customer_name == null && item.customer_phone == null) {
                itemView.tv_no_cus.visibility = View.VISIBLE
                itemView.tv_cus_name.visibility = View.GONE
                itemView.tv_cus_phone.visibility = View.GONE
            } else {
                itemView.tv_no_cus.visibility = View.GONE
                itemView.tv_cus_name.visibility = View.VISIBLE
                itemView.tv_cus_phone.visibility = View.VISIBLE
                itemView.tv_cus_name.text = item.customer_name
                itemView.tv_cus_phone.text =
                    context.getString(R.string.customer_phone, item.customer_phone)
            }

            itemView.tv_total_amount.text =
                context.getString(R.string.price, formatMoney(item.total_amount_receive.toLong()))
            itemView.tv_dd_mm.text = context.getString(
                R.string.dd_mm,
                item.created_at.subSequence(8, 10),
                item.created_at.subSequence(5, 7)
            )
            itemView.tv_yyyy.text = item.created_at.subSequence(0, 4)
        }
    }
}