package com.example.project.fragment.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.project.R
import com.example.project.base.BaseFragment
import com.example.project.constraint.Constraint
import com.example.project.constraint.Constraint.Companion.KEY_PRODUCT_DETAIL
import com.example.project.constraint.Constraint.Companion.loadProductList
import com.example.project.constraint.Constraint.Companion.selectedTabResume
import com.example.project.navigation.MyNavigation
import com.example.project.utils.MessageBox
import com.example.project.utils.RecyclerItemCLickListener
import com.example.project.utils.setOnLoadMore
import kotlinx.android.synthetic.main.fragment_product.*

/**
 * A simple [Fragment] subclass.
 */
class ProductFragment : BaseFragment() {
    lateinit var viewModel: VMProductFragment
    lateinit var productAdapter: ProductAdapter
    lateinit var layoutManager: RecyclerView.LayoutManager
    private var pageIndex = 1
    private var product_name = ""
    private var code = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product, container, false)
    }

    override fun onResume() {
        super.onResume()
        if (loadProductList) {
            loadAll()
        }
    }

    fun loadAll() {
        pageIndex = 1
        init()
        event()
        dataListener()
        fetchData()
        loadProductList = false
    }

    fun init() {
        viewModel = ViewModelProviders.of(this@ProductFragment).get(VMProductFragment::class.java)
        viewModel.init(context!!)

        productAdapter = ProductAdapter(context!!)
        layoutManager = GridLayoutManager(context!!, 2)
        rv_product.adapter = productAdapter
        rv_product.layoutManager = layoutManager
    }

    fun event() {
        edt_key.setOnClickListener {
            ll_clear_filter.visibility = View.VISIBLE
            btn_search.setOnClickListener {
                if (edt_key.text.toString() == "") {
                    MessageBox().showLongToast(
                        context!!,
                        context!!.getString(R.string.require_input_key)
                    )
                } else {
                    product_name = edt_key.text.toString()
                    loadAll()
                }
            }
            btn_clear_filter.setOnClickListener {
                edt_key.setText("")
                product_name = ""
                loadAll()
                ll_clear_filter.visibility = View.GONE
            }
        }

        btn_add_product.setOnClickListener {
            MyNavigation.getInstance().goToAddProduct()
            KEY_PRODUCT_DETAIL = "add"
        }

        rv_product.addOnItemTouchListener(
            RecyclerItemCLickListener(
                context!!,
                rv_product,
                object :
                    RecyclerItemCLickListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        if (viewModel.listProduct.value!![position].product_qty.toInt() > 0) {
                            Constraint.currentProduct = viewModel.listProduct.value!![position]
                            MyNavigation.getInstance().goToProductDetail()
                        } else {
                            MessageBox().showLongToast(
                                context,
                                context!!.getString(R.string.noti_out_of_product)
                            )
                        }
                    }

                    override fun onLongClick(view: View?, position: Int) {
                    }
                })
        )

        rv_product.setOnLoadMore {
            if (viewModel.listProduct.value!!.size >= 10) {
                pageIndex++
                viewModel.getListProduct(pageIndex.toString(), product_name, code)
            }
        }
    }

    fun fetchData() {
        viewModel.getListProduct(pageIndex.toString(), product_name, code)
    }

    fun dataListener() {
        viewModel.loadingView.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                showLoading()
            } else dismissLoading()
        })

        viewModel.listProduct.observe(viewLifecycleOwner, Observer {
            if (it.size > 0) {
                productAdapter.refresh(it)
            } else {
                rv_product.visibility = View.GONE
                tv_no_data.visibility = View.VISIBLE
            }
        })
    }
}
