package com.example.project.fragment.customer

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.currentCustomer
import com.example.project.http.API
import com.example.project.model.Customer
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody

class VMCustomerFragment : ViewModel() {
    lateinit var loadingView: MutableLiveData<Int>
    lateinit var listCustomer: MutableLiveData<ArrayList<Customer>>
    var tmpData = ArrayList<Customer>()
    lateinit var body: HashMap<String, String>
    lateinit var context: Context
    var totalPage: Int = 1
    lateinit var addCustomerSuccess: MutableLiveData<Boolean>

    fun init(context: Context) {
        this.context = context
        this.loadingView = MutableLiveData(View.GONE)
        listCustomer = MutableLiveData()
        addCustomerSuccess = MutableLiveData()
    }

    fun getListCustomer(page: String, customer_name: String, customer_phone: String) {
        loadingView.value = View.VISIBLE
        body = hashMapOf(
            "page" to page,
            "customer_name" to customer_name,
            "customer_phone" to customer_phone
        )

        API.apiService.getListCustomer(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    totalPage = it.total_page
                    if (it.result != null) {
                        if (page.toInt() == 1) {
                            tmpData.clear()
                        }
                        tmpData.addAll(it.result!!)
                        listCustomer.value = tmpData
                    }
                } else {
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                }
            }, {
                loadingView.value = View.GONE
                LogUtil.debug("__err_get_list_cus__", it.toString())
                MessageBox().showLongToast(context, context.getString(R.string.err_internet))
            }).isDisposed
    }

    fun addCustomer(customer_name: String, customer_phone: String) {
        loadingView.value = View.VISIBLE

        val c_customer_name = RequestBody.create(MediaType.parse("text/plain"), customer_name)
        val c_customer_phone = RequestBody.create(MediaType.parse("text/plain"), customer_phone)

        API.apiService.addCustomer(
            c_customer_name,
            c_customer_phone
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    addCustomerSuccess.value = true
                    MessageBox().showLongToast(context, it.message)
                } else MessageBox().showAlert(
                    context,
                    it.message,
                    context.getString(R.string.ok)
                )
            }, {
                loadingView.value = View.GONE
                LogUtil.debug("__err_add_cus__", it.toString())
                MessageBox().showLongToast(context, context.getString(R.string.err_internet))
            }).isDisposed
    }

    fun editCustomer(
        customer_name: String,
        customer_phone: String,
        customer_point: String
    ) {
        loadingView.value = View.VISIBLE

        val c_customer_name = RequestBody.create(MediaType.parse("text/plain"), customer_name)
        val c_customer_phone = RequestBody.create(MediaType.parse("text/plain"), customer_phone)
        val c_customer_point = RequestBody.create(MediaType.parse("text/plain"), customer_point)
        val c_customer_id = RequestBody.create(
            MediaType.parse("text/plain"),
            currentCustomer!!.customer_id.toString()
        )

        API.apiService.editCustomer(
            c_customer_name,
            c_customer_phone,
            c_customer_point,
            c_customer_id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    addCustomerSuccess.value = true
                    MessageBox().showLongToast(context, it.message)
                } else MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
            }, {
                loadingView.value = View.GONE
                LogUtil.debug("__err_edit_cus__", it.toString())
                MessageBox().showLongToast(context, context.getString(R.string.err_internet))
            }).isDisposed
    }
}
