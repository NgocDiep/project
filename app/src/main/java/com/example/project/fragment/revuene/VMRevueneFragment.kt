package com.example.project.fragment.revuene

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.currentHistoryOrder
import com.example.project.http.API
import com.example.project.model.HistoryOrder
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class VMRevueneFragment : ViewModel() {
    lateinit var loadingView: MutableLiveData<Int>
    lateinit var listHistoryOrder: MutableLiveData<ArrayList<HistoryOrder>>
    var tmpData = ArrayList<HistoryOrder>()
    lateinit var body: HashMap<String, String>
    lateinit var context: Context
    var totalPage: Int = 1
    lateinit var getOrderDetailSuccess: MutableLiveData<Boolean>

    fun init(context: Context) {
        this.context = context
        this.loadingView = MutableLiveData(View.GONE)
        listHistoryOrder = MutableLiveData()
        getOrderDetailSuccess = MutableLiveData()
    }

    fun getListHistoryOrder(page: String, customer_name: String) {
        loadingView.value = View.VISIBLE
        body = hashMapOf(
            "page" to page,
            "customer_name" to customer_name,
            "status" to ""
        )

        API.apiService.getListHistoryOrder(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    totalPage = it.total_page
                    if (it.result != null) {
                        if (page.toInt() == 1) {
                            tmpData.clear()
                        }
                        tmpData.addAll(it.result!!)
                        listHistoryOrder.value = tmpData
                    }
                } else {
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
                }
            }, {
                loadingView.value = View.GONE
                LogUtil.debug("__err_get_list_history_order__", it.toString())
                MessageBox().showLongToast(context, context.getString(R.string.err_internet))
            }).isDisposed
    }

    fun getHistoryOrderDetail(id: Int) {
        loadingView.value = View.VISIBLE
        API.apiService.getHistoryOrderDetail(id).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingView.value = View.GONE
                if (it.status == "success") {
                    if (it.result != null) {
                        currentHistoryOrder = it.result!!
                    }
                    getOrderDetailSuccess.value = true
                } else MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
            }, {
                loadingView.value = View.GONE
                LogUtil.debug("__err_get_history_order_detail__", it.toString())
                MessageBox().showAlert(
                    context,
                    context.getString(R.string.err_internet),
                    context.getString(R.string.ok)
                )
            }).isDisposed
    }
}