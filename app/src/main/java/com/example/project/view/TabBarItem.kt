package com.example.project.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.project.R
import com.example.project.extensions.dp
import com.example.project.extensions.getColorResource

class TabBarItem : LinearLayout {

    private var activeIcon: Drawable? = null
    private var inactiveIcon: Drawable? = null

    private var title = ""

    private lateinit var imageView : ImageView
    private lateinit var textView: TextView

    private var isActive = false

    constructor(context: Context) : super(context) {init(context, null)}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {init(context, attrs)}

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        attrs?.let {
            obtainValue(context, attrs)
        }
        createIcon()
        createTitle()
        orientation = VERTICAL
        gravity = Gravity.CENTER
    }

    private fun obtainValue(context: Context, attrs: AttributeSet?) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.TabBarItem)
        activeIcon = ta.getDrawable(R.styleable.TabBarItem_activeIcon)
        inactiveIcon = ta.getDrawable(R.styleable.TabBarItem_inactiveIcon)
        title = ta.getString(R.styleable.TabBarItem_title) ?: ""
        ta.recycle()
    }

    private fun createIcon() {
        imageView = ImageView(context)
        val size = context.dp(24f)
        val params = LayoutParams(size, size)
        params.bottomMargin = context.dp(5f)
        imageView.layoutParams = params
        imageView.setImageDrawable(if (isActive) {
            activeIcon
        } else {
            inactiveIcon
        })
        addView(imageView)
    }

    private fun createTitle() {
        textView = LayoutInflater.from(context).inflate(R.layout.layout_tabbar_title, this, false) as TextView
        textView.text = title
        textView.setTextSize(11f)
        addView(textView)
    }

    fun setActive(isActive: Boolean) {
        if (isActive) {
            imageView.setImageDrawable(activeIcon)
            textView.setTextColor(context.getColorResource(R.color.color_app))
        } else {
            imageView.setImageDrawable(inactiveIcon)
            textView.setTextColor(context.getColorResource(R.color.color_grey_1))
        }
    }
}
