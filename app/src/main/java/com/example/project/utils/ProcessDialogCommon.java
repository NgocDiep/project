package com.example.project.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.example.project.R;

public class ProcessDialogCommon {
    static ProgressDialog progressDialog;
    static boolean isShowing = false;

    public ProcessDialogCommon() {

    }

    public static void showdialog(Context context, boolean cancelable) {
        if (context != null && !isShowing) {
            progressDialog = new ProgressDialog(context);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.show();
            progressDialog.setContentView(R.layout.layout_loading);
            progressDialog.setProgressStyle(R.style.LoadingView);
            progressDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setCancelable(cancelable);
            progressDialog.getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            progressDialog.setCanceledOnTouchOutside(true);
            isShowing = true;
        }


    }

    public static void closeDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            isShowing = false;
        } catch (Exception ex) {
            Log.i("processDialog", "close Error" + ex);
        }
    }
}
