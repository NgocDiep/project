package com.example.project.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.project.R
import com.example.project.extensions.alert
import java.util.*

object CheckPermission {
    val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123
    val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1
    val MY_PERMISSIONS_REQUEST_CAMERA = 3
    val CAMERAandWRITE_EXTERNAL_STORAGE = 2

    fun checkPermissionREAD_EXTERNAL_STORAGE(activity: Activity?): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        activity,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                ) {
                    ActivityCompat
                        .requestPermissions(
                            activity,
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                        )

                } else {
                    showDialog(
                        activity.getString(R.string.permission_read_lib), activity,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                    )
                }
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    fun checkPermissionREAD_EXTERNAL_STORAGE(fragment: Fragment): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    fragment.activity!!,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                fragment.requestPermissions(
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                )
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    fun showAlertPermissionREAD_EXTERNAL_STORAGE(activity: Activity?) {
//        val checkShowPhoneBook = ActivityCompat.shouldShowRequestPermissionRationale(
//            activity!!,
//            Manifest.permission.READ_EXTERNAL_STORAGE
//        )
//        if (checkShowPhoneBook) {
//            activity.alert("Bạn cần cho phép Vayonline247 truy cập vào ảnh, phương tiện và tệp trên thiết bị của bạn")
//        } else {
//            activity.alert("Bạn cần vào CÀI ĐẶT của thiết bị và cấp lại quyền Bộ nhớ")
//        }
    }

    fun checkPermissionWRITE_EXTERNAL_STORAGE(activity: Activity?): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                ) {
                    ActivityCompat
                        .requestPermissions(
                            activity,
                            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
                        )

                } else {
                    showDialog(
                        activity.getString(R.string.permission_read_lib), activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                    )
                }
                false
            } else {
                true
            }
        } else {
            true
        }
    }


    fun checkPermissionCAMERA(activity: Activity?): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat
                    .requestPermissions(
                        activity,
                        arrayOf(Manifest.permission.CAMERA),
                        MY_PERMISSIONS_REQUEST_CAMERA
                    )
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    fun showAlertPermissionCAMERA(activity: Activity?) {
        val checkPermissionCamera = ActivityCompat.shouldShowRequestPermissionRationale(
            activity!!,
            Manifest.permission.CAMERA
        )
        activity.let {
            if (checkPermissionCamera) {
                it.alert("Để tiếp tục, bạn cần cho phép My Store truy cập Camera") {
                    activity.onBackPressed()
                }
            } else it.alert("Để tiếp tục, bạn cần vào CÀI ĐẶT của thiết bị và cấp lại quyền Camera") {
                activity.onBackPressed()
            }
        }
    }

    fun checkPermissionCAMERA(fragment: Fragment): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    fragment.activity!!,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                fragment.requestPermissions(
                    arrayOf(Manifest.permission.CAMERA),
                    MY_PERMISSIONS_REQUEST_CAMERA
                )
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    fun showAlertPermissionCAMERA(fragment: Fragment) {
        val checkPermissionCamera = ActivityCompat.shouldShowRequestPermissionRationale(
            fragment.activity!!,
            Manifest.permission.CAMERA
        )
        if (checkPermissionCamera) {
            fragment.activity!!.alert("Để tiếp tục, bạn cần cho phép My Store truy cập Camera")
        } else fragment.activity!!.alert("Để tiếp tục, bạn cần vào CÀI ĐẶT của thiết bị và cấp lại quyền Camera")
    }

    fun checkPermissionCAMERAandWRITE_EXTERNAL_STORAGE(
        activity: Activity?,
        packageManager: PackageManager
    ): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            val permissions = ArrayList<String>()
            var permissionsToRequest = ArrayList<String>()
            permissions.add(Manifest.permission.CAMERA)
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            permissionsToRequest = permissionsToRequest(permissions, activity)

            if (permissionsToRequest!!.size > 0) {
                ActivityCompat.requestPermissions(
                    activity!!,
                    permissionsToRequest!!.toTypedArray(),
                    CAMERAandWRITE_EXTERNAL_STORAGE
                )
                return false
            } else {
                return true
            }
        } else {
            true
        }
    }

    private fun permissionsToRequest(
        wantedPermissions: ArrayList<String>,
        activity: Activity?
    ): ArrayList<String> {
        val result = ArrayList<String>()

        for (perm in wantedPermissions) {
            if (!hasPermission(perm, activity)) {
                result.add(perm)
            }
        }
        return result
    }

    private fun hasPermission(permission: String, activity: Activity?): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.let {
                it!!.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
            }
        } else true
    }

    private fun showDialog(
        msg: String,
        context: Context?,
        permission: String,
        permissionCode: Int
    ) {
        val alertBuilder =
            AlertDialog.Builder(context!!)
        alertBuilder.setCancelable(true)
        alertBuilder.setTitle("Yêu cầu truy cập")
//        alertBuilder.setMessage("$msg permission is necessary")
        alertBuilder.setMessage("$msg")
        alertBuilder.setPositiveButton(
            android.R.string.yes
        ) { dialog, which ->
            ActivityCompat.requestPermissions(
                (context as Activity?)!!, arrayOf(permission),
                permissionCode
            )
        }
        val alert = alertBuilder.create()
        alert.show()
    }

    private fun showDialog(msg: String, context: Context?) {
        val alertBuilder =
            AlertDialog.Builder(context!!)
        alertBuilder.setCancelable(true)
        alertBuilder.setTitle("Yêu cầu truy cập")
//        alertBuilder.setMessage("$msg permission is necessary")
        alertBuilder.setMessage("$msg")
        alertBuilder.setPositiveButton(
            android.R.string.yes
        ) { dialog, which ->
            //            ActivityCompat.requestPermissions(
//                (context as Activity?)!!, arrayOf(permission),
//                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
//            )
        }
        val alert = alertBuilder.create()
        alert.show()
    }
}