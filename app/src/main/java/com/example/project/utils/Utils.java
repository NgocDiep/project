package com.example.project.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
	public Utils() {

	}


	public static boolean isNullOrEmpty(String input) {
		if (input == null)
			return true;
		if (input.length() == 0)
			return true;
		return false;
	}

	public static boolean checkLength(String input, int minMumber, int maxNumber) {
		if (input.length() < minMumber || input.length() > maxNumber)
			return false;

		return true;
	}

	public static boolean minLength(String input, int minMumber) {
		if (input.length() < minMumber)
			return false;

		return true;
	}
	public static boolean maxLength(String input, int maxNumber) {
		if ( input.length() > maxNumber)
			return false;

		return true;
	}

	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public static String urlFormat(String url) {
		url = url.replace(" ", "%20");
		return url;
	}
	public static void finishActivity(Activity activity) {
		if (activity != null){
			activity.finish();
		}
	}

	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	private static final int FIX_RATIO = 4; // scale ratio
	private static final long MB_TO_BYTES = 1024 * 1024;

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static boolean isFree(String _price) {
		if (_price == null)
			return true;

		int price = 0;
		try {
			price = Integer.valueOf(_price);
			if (price > 0)
				return false;
			else {
				return true;
			}
		} catch (Exception e) {
			return true;
		}
	}

	public static String StringFormatHttp(String text) {
		text = text.replaceAll(" ", "%20");
		return text;
	}

	@SuppressLint("DefaultLocale")
	public static String durationInSecondsToString(int sec) {
		int hours = sec / 3600;
		int minutes = (sec / 60) - (hours * 60);
		int seconds = sec - (hours * 3600) - (minutes * 60);
		String formatted = String.format("%d:%02d:%02d", hours, minutes, seconds);
		return formatted;
	}

	private static final Pattern MSISDN_PATTERN = Pattern.compile("84[0-9]{9,10}");

	private static final Pattern SERI_PATTERN = Pattern.compile("[0-9]{12,15}");

	public static boolean isValidSeri(String seri) {
		if (seri == null) {
			return false;
		}
		return SERI_PATTERN.matcher(seri).matches();
	}

	public static boolean isValidMsisdn(String msisdn) {
		if (msisdn == null) {
			return false;
		}
		return MSISDN_PATTERN.matcher(msisdn).matches();
	}

	public static String normalizeMsisdn(String msisdn) {
		if (msisdn == null)
			return null;
		msisdn = msisdn.replaceAll("\\s", "");
		if (msisdn.startsWith("+")) {
			msisdn = msisdn.substring(1);
		}

		if (msisdn.startsWith("00")) {
			return msisdn.substring(2);
		}

		if (msisdn.startsWith("0")) {
			return "84" + msisdn.substring(1);
		}

		return msisdn;
	}

	public static void killApp(boolean killSafely) {
		if (killSafely) {
			System.runFinalizersOnExit(true);
			System.exit(0);
		} else {
			android.os.Process.killProcess(android.os.Process.myPid());
		}
	}

	public static int getWidth1(Context ctx) {
		Display display = ((WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

		return display.getWidth();
	}


	public static int getWidth(Context ctx){

		DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();

		return metrics.widthPixels;
	}

	public static int getHeight(Context ctx) {
		Display display = ((WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

		return display.getHeight();
	}

	private static final String preHtml = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">"
			+ "<style type=\"text/css\">" + "body { background-color: %s; color: %s; font-size: %s;} img{width: %s;}"
			+ "</style></head><body>";
	private static final String postHtml = "</body></html>";

	public static String generateHtml(String content, int width, int fontsize, boolean isLight) {

		String backgroundColor = "transparent";// isLight ? "#ffffff" :
												// "#111111";
		String foregroundColor = "#111111";// isLight ? "#111111" : "#ffffff";

		return String.format(preHtml, backgroundColor, foregroundColor, String.valueOf(fontsize) + "px",
				String.valueOf(width - 10) + "px") + content + postHtml;
	}

	// img{display: inline;height: auto;max-width: %s;}
	// img { display: block;margin: 0 auto;}
	private static final String preHtml2 = "<html><head><meta http-equiv=\"Content-Type\" content=\"width=device-width; user-scalable=yes; text/html; charset=utf-8\">"
			+ "<style type=\"text/css\">"
			+ "body { background-color: transparent;} img { display: block; margin: 0 auto; height: auto;max-width: %s;}"
			+ "</style></head><body>";
	private static final String preHtml3 = "<html><head><meta http-equiv=\"Content-Type\" content=\"width=device-width; user-scalable=yes; text/html; charset=utf-8\">"
			+ "<style type=\"text/css\">"
			+ "body { background-color: while;} img { display: block; margin: 0 auto; height: auto;max-width: %s;}"
			+ "</style></head><body>";
	public static String generateHtml2(String content, int width) {

		return String.format(preHtml2, "100%") + content + postHtml;
	}

	public static String generateHtml3(String content, int width) {

		return String.format(preHtml3, "100%") + content + postHtml;
	}
	private static final DateFormat LONG_DATETIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static final DateFormat FULL_DATETIME = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public static String formatLongDateTime(Date date) {
		return LONG_DATETIME.format(date);
	}

	public static String formatFullDateTime(Date date) {
		return FULL_DATETIME.format(date);
	}

	public static final DateFormat DAY_MONTH_YEAR = new SimpleDateFormat("dd/MM/yyyy");

	public static String formatDayMonthYear(Date date) {
		return DAY_MONTH_YEAR.format(date);
	}

	/**
	 * string to date
	 * 
	 * @param input
	 * @return
	 */
	public static Date stringToLongDate(String input) {
		try {
			return LONG_DATETIME.parse(input);
		} catch (Exception e) {
		}

		return null;
	}

	private static final DateFormat SHORT_DATETIME = new SimpleDateFormat("dd/MM HH:mm");

	/**
	 * format short date
	 * 
	 * @param date
	 * @return
	 */
	public static String formatShortDateTime(Date date) {
		return SHORT_DATETIME.format(date);
	}

	/**
	 * format short date from string
	 * 
	 * @param input
	 * @return
	 */
	public static String stringToShortDate(String input) {
		Date d = null;
		d = stringToLongDate(input);
		if (d != null)
			return SHORT_DATETIME.format(d);
		return input;
	}

	// public static float dpFromPx(final Context context, final float px) {
	// return px / context.getResources().getDisplayMetrics().density;
	// }
	//
	// public static float pxFromDp(final Context context, final float dp) {
	// return dp * context.getResources().getDisplayMetrics().density;
	// }

	/**
	 * This method converts dp unit to equivalent pixels, depending on device
	 * density.
	 * 
	 * @param dp
	 *            A value in dp (density independent pixels) unit. Which we need
	 *            to convert into pixels
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent px equivalent to dp depending on
	 *         device density
	 */
	public static float convertDpToPixel(float dp, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.density);
		return px;
	}

	/**
	 * This method converts device specific pixels to density independent
	 * pixels.
	 * 
	 * @param px
	 *            A value in px (pixels) unit. Which we need to convert into db
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent dp equivalent to px value
	 */
	public static float convertPixelsToDp(float px, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.density);
		return dp;
	}

	public static String getVersionName(Context context, @SuppressWarnings("rawtypes") Class cls) {
		try {
			ComponentName comp = new ComponentName(context, cls);
			PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
			return pinfo.versionName;
		} catch (PackageManager.NameNotFoundException e) {
			return "";
		}
	}

	public static int getVersionCode(Context context, @SuppressWarnings("rawtypes") Class cls) {
		try {
			ComponentName comp = new ComponentName(context, cls);
			PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
			return pinfo.versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			return 0;
		}
	}

//	public static String getDeviceId(Context context) {
//		String androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
//		if ((androidId == null) || (androidId.equals("9774d56d682e549c")) || (androidId.equals("0000000000000000"))) {
//			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
//			androidId = prefs.getString(CMDKey.DEVICE_ID, null);
//		}
//		return androidId;
//	}

//	public static ScreenInfo getScreenInfo(Context ctx) {
//		WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
//		DisplayMetrics metrics = new DisplayMetrics();
//		wm.getDefaultDisplay().getMetrics(metrics);
//		ScreenInfo si = new ScreenInfo();
//		si.h = metrics.heightPixels;
//		si.w = metrics.widthPixels;
//
//		int resStatusBarId = ctx.getResources().getIdentifier("status_bar_height", "dimen", "android");
//		if (resStatusBarId > 0) {
//			si.h = si.h - ctx.getResources().getDimensionPixelSize(resStatusBarId);
//		}
//
//		return si;
//	}
	
	public static DisplayMetrics getDisplayMetrics(Context ctx ) {
		DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();

		return metrics;
	}

	public static void hideSoftKeyboard(Activity ctx) {
		try {

			InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(ctx.getCurrentFocus().getWindowToken(), 0);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * Show soft keyboard
	 * 
	 * @param ctx
	 */
	public static void showSoftKeyboard(Activity ctx) {
		try {
			InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(ctx.getCurrentFocus(), 0);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

//	public static NKMTApplication getMyApplication(Context ctx) {
//		return (NKMTApplication) ctx.getApplicationContext();
//	}

	public static int random(int min, int max) {
		try {
			Random rn = new Random();
			int range = max - min + 1;
			int randomNum = min + rn.nextInt(range);
			return randomNum;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public static String encodeTobase64(Bitmap image) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.JPEG, 80, baos);
		byte[] b = baos.toByteArray();
		String imageEncoded = Base64.encodeBytes(b);
		return imageEncoded;
	}

	// decodes image and scales it to reduce memory consumption
	public static Bitmap decodeFile(File f, int requiredSize) {
		try {
			// System.err.println("decodeFile");

			// decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// Find the correct scale value. It should be the power of 2.
			final int REQUIRED_SIZE = requiredSize;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			// System.err.println("decodeFile
			// outWidth:"+o.outWidth+",outHeight:"+o.outHeight);

			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale++;
			}
//			if (f.length() > 3 * MB_TO_BYTES) {
//				scale = 8;
//			} else if ((f.length() > MB_TO_BYTES) && (f.length() <= 3 * MB_TO_BYTES)) {
//				scale = 4;
//			} else {
//				scale = 2;
//			}
			// scale=FIX_RATIO;
			// System.err.println("decodeFile 3");
			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
			// System.err.println("decodeFile FileNotFoundException:"
			// + e.toString());
		}
		return null;
	}

	public static String prepareUploadImage(Context ctx, String file_path) {
		// Check null filepath
		if (Utils.isNullOrEmpty(file_path))
			return "";

		// Create bufferedInput
		File f = new File(file_path);
		if (!f.exists() || !f.isFile()) {
			return "";
		}

		int size = (int) (f.length() / 1024);

		// Max 10M
		if (size > 10240) {
//			MessageBox.showLongToast(ctx, ctx.getString(R.string.msg_file_maximum_error));
			return "";
		}

		try {
			Bitmap b = Utils.decodeFile(f, 200);
			if (b != null) {
				ExifInterface exif;
				try {
					exif = new ExifInterface(f.getAbsolutePath());

					int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
					Log.d("EXIF", "Exif: " + orientation);
					Matrix matrix = new Matrix();
					if (orientation == 6) {
						matrix.postRotate(90);
						Log.d("EXIF", "Exif: " + orientation);
					} else if (orientation == 3) {
						matrix.postRotate(180);
						Log.d("EXIF", "Exif: " + orientation);
					} else if (orientation == 8) {
						matrix.postRotate(270);
						Log.d("EXIF", "Exif: " + orientation);
					}
					b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
				} catch (IOException e) {
					e.printStackTrace();
				}
				return Utils.encodeTobase64(b);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
//			MessageBox.showToast(ctx, ex.getMessage());
		}

		return "";
	}


	// dungnt

	public String getFilename() {
		File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
		if (!file.exists()) {
			file.mkdirs();
		}
		String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
		return uriSting;

	}

	private String getRealPathFromURI(Context context, String contentURI) {
		Uri contentUri = Uri.parse(contentURI);
		Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
		if (cursor == null) {
			return contentUri.getPath();
		} else {
			cursor.moveToFirst();
			int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			return cursor.getString(index);
		}
	}

	public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		final float totalPixels = width * height;
		final float totalReqPixelsCap = reqWidth * reqHeight * 2;
		while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
			inSampleSize++;
		}

		return inSampleSize;
	}

	public String compressImage(Context context, String imageUri) {

		String filePath = getRealPathFromURI(context, imageUri);
		Bitmap scaledBitmap = null;

		BitmapFactory.Options options = new BitmapFactory.Options();

		// by setting this field as true, the actual bitmap pixels are not
		// loaded in the memory. Just the bounds are loaded. If
		// you try the use the bitmap here, you will get null.
		options.inJustDecodeBounds = true;
		Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

		int actualHeight = options.outHeight;
		int actualWidth = options.outWidth;

		// max Height and width values of the compressed image is taken as
		// 816x612

		float maxHeight = 816.0f;
		float maxWidth = 612.0f;
		float imgRatio = actualWidth / actualHeight;
		float maxRatio = maxWidth / maxHeight;

		// width and height values are set maintaining the aspect ratio of the
		// image

		if (actualHeight > maxHeight || actualWidth > maxWidth) {
			if (imgRatio < maxRatio) {
				imgRatio = maxHeight / actualHeight;
				actualWidth = (int) (imgRatio * actualWidth);
				actualHeight = (int) maxHeight;
			} else if (imgRatio > maxRatio) {
				imgRatio = maxWidth / actualWidth;
				actualHeight = (int) (imgRatio * actualHeight);
				actualWidth = (int) maxWidth;
			} else {
				actualHeight = (int) maxHeight;
				actualWidth = (int) maxWidth;

			}
		}

		// setting inSampleSize value allows to load a scaled down version of
		// the original image

		options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

		// inJustDecodeBounds set to false to load the actual bitmap
		options.inJustDecodeBounds = false;

		// this options allow android to claim the bitmap memory if it runs low
		// on memory
		options.inPurgeable = true;
		options.inInputShareable = true;
		options.inTempStorage = new byte[16 * 1024];

		try {
			// load the bitmap from its path
			bmp = BitmapFactory.decodeFile(filePath, options);
		} catch (OutOfMemoryError exception) {
			exception.printStackTrace();

		}
		try {
			scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
		} catch (OutOfMemoryError exception) {
			exception.printStackTrace();
		}

		float ratioX = actualWidth / (float) options.outWidth;
		float ratioY = actualHeight / (float) options.outHeight;
		float middleX = actualWidth / 2.0f;
		float middleY = actualHeight / 2.0f;

		Matrix scaleMatrix = new Matrix();
		scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

		Canvas canvas = new Canvas(scaledBitmap);
		canvas.setMatrix(scaleMatrix);
		canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2,
				new Paint(Paint.FILTER_BITMAP_FLAG));

		// check the rotation of the image and display it properly
		ExifInterface exif;
		try {
			exif = new ExifInterface(filePath);

			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
			Log.d("EXIF", "Exif: " + orientation);
			Matrix matrix = new Matrix();
			if (orientation == 6) {
				matrix.postRotate(90);
				Log.d("EXIF", "Exif: " + orientation);
			} else if (orientation == 3) {
				matrix.postRotate(180);
				Log.d("EXIF", "Exif: " + orientation);
			} else if (orientation == 8) {
				matrix.postRotate(270);
				Log.d("EXIF", "Exif: " + orientation);
			}
			scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(),
					matrix, true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		FileOutputStream out = null;
		String filename = getFilename();
		try {
			out = new FileOutputStream(filename);

			// write the compressed bitmap at the destination specified by
			// filename.
			scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return filename;

	}

}
