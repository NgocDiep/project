package com.example.project.utils

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import com.example.project.R

class LoadingView(context: Context) :
    Dialog(context, R.style.LoadingView) {
    init {
        setContentView(R.layout.layout_loading)
        window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
    }
}
