package com.example.project.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File


object FileImage {
    fun decodeImageFromFiles(path: String, width: Int, height: Int): Bitmap {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        var scale = 1
        //Giam size anh scale lan, scale phai la boi cua 2
        while (scaleOptions.outWidth / scale / 2 >= width && scaleOptions.outHeight / scale / 2 >= height) {
            scale *= 2
        }
        // decode with the sample size
        val outOptions = BitmapFactory.Options()
        outOptions.inSampleSize = scale
        val decodeBitmap = BitmapFactory.decodeFile(path, outOptions)

        if (scaleOptions.outHeight > scaleOptions.outWidth) {
            val matrix = Matrix()
//            matrix.postRotate(-90.0F)// 180/8
            return Bitmap.createBitmap(
                decodeBitmap,
                0,
                0,
                decodeBitmap.width,
                decodeBitmap.height,
                matrix,
                true
            )
        } else {
            // rotate image
//            val matrix = Matrix()
//            if (scaleOptions.outWidth >= 4000){
//                matrix.postRotate(0.0F)// 0
//            }else{
//                matrix.postRotate(180.0F)// 180/8
//            }
//            return Bitmap.createBitmap(decodeBitmap, 0, 0, decodeBitmap.width, decodeBitmap.height, matrix, true)
            return checkRotate(path, decodeBitmap)!!
        }
    }

    fun checkFileImageNull(path: String?): Boolean {
        val scaleOptions = BitmapFactory.Options()
        scaleOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, scaleOptions)
        return scaleOptions.outWidth > 0
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
//        matrix.postRotate(angle -90F) /// dong bo voi ios
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    fun checkRotate(photoPath: String, bitmap: Bitmap): Bitmap? {
        var rotatedBitmap: Bitmap? = null
        try {
            val ei = ExifInterface(photoPath)
            val orientation: Int = ei.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )


            rotatedBitmap = when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90F)
                ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180F)
                ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270F)
                ExifInterface.ORIENTATION_NORMAL -> bitmap
                else -> bitmap
            }

        } catch (e: Exception) {

        }
        if (rotatedBitmap != null) {
            return rotatedBitmap
        } else {
            return bitmap
        }
    }

    private var oldFile: File? = null
    fun deleteFile(path: String?) {
        try {
            if (!Utils.isNullOrEmpty(path)) {
                Single.fromCallable {
                    oldFile = File(path)
                    oldFile?.delete()
                }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
            }
        } catch (e: Throwable) {
            LogUtil.debug("__err_delete_file__", e.toString())
        }
    }
}