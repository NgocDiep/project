package com.example.project.utils

import android.content.Context
import com.example.project.R
import java.lang.StringBuilder
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

fun formatMoney(value: Long): String {
    val formatter = DecimalFormat("#,###")
    return formatter.format(value).replace(',', '.')
}

fun moneyToText(value: Long) : String{
    val arr = arrayOf(
        "",
        "một",
        "hai",
        "ba",
        "bốn",
        "năm",
        "sáu",
        "bảy",
        "tám",
        "chín"
    )

    val arr2 = arrayOf(
        "",
        "nghìn",
        "triệu",
        "tỷ"
    )

    val arr3 = arrayOf(
        "",
        "nghìn"
    )
    val length = value.toString().length

    var strings = arrayListOf<String>()
    var i = 0
    val string = value.toString().reversed()
    do {
        val start = i * 12
        var end = (i + 1) * 12
        if (end > length) {
            end = length
        }
        strings.add(string.substring(start, end))
        i += 1
    } while (12 * i < length)
    val words = arrayListOf<String>()
    strings.forEachIndexed {index1, str ->
        var list = arrayListOf<String>()
        var length = str.length
        i = 0
        while (i * 3 < length) {
            val s = i * 3
            var e = (i + 1) * 3
            if (e > length) {
                e = length
            }
            list.add(str.substring(s, e).reversed())
            i += 1
        }
        words.add(arr3[index1])
        list.forEachIndexed { index, it ->
            val s = StringBuilder()
            val l = it.length
            if (l == 3) {
                val a = it[0].toString().toInt()
                if (a > 0) {
                    s.append(arr[a])
                    s.append(" trăm ")
                }
                val b = it[1].toString().toInt()
                if (b == 1) {
                    s.append(" mười ")
                }
                if (b > 1) {
                    s.append(arr[b])
                    s.append(" mươi ")
                }
                val c = it[2].toString().toInt()
                if (c > 0) {
                    if (b != 0) {
                        when (c) {
                            5 -> s.append("lăm")
                            1 -> s.append("mốt")
                            else -> s.append(arr[c])
                        }
                    } else {
                        s.append(" linh ")
                        s.append(arr[c])
                    }
                }
            } else if (l == 2) {
                val a = it[0].toString().toInt()
                if (a > 0) {
                    s.append(arr[a])
                    s.append(" mươi ")
                }
                val b = it[1].toString().toInt()
                if (b > 0) {
                    if (a != 0) {
                        when (a) {
                            5 -> s.append("lăm")
                            1 -> s.append("mốt")
                            else -> s.append(arr[b])
                        }
                    } else {
                        s.append(" linh ")
                        s.append(arr[b])
                    }
                }
            } else if (l == 1) {
                val a = it[0].toString().toInt()
                if (a > 0) {
                    s.append(arr[a])
                }
            }
            if (s.trim() != "") {
                words.add(arr2[index])
                words.add(s.toString())
            }
        }
    }
    return words.reversed().joinToString(" ").replace(regex = Regex("\\s+"), replacement = " ")
}


fun formatDate(text: String) : String{
    val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
    val date = sdf.parse(text)
    val dateFormat = SimpleDateFormat("dd MMM, yyyy", Locale.getDefault())
    return dateFormat.format(date)
}

fun formatDate(text: String, format: String) : String{
    val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
    val date = sdf.parse(text)
    val dateFormat = SimpleDateFormat(format, Locale.getDefault())
    return dateFormat.format(date)
}


fun formatDateyyyyMMdd(text: String, format: String) : String{
    val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val date = sdf.parse(text)
    val dateFormat = SimpleDateFormat(format, Locale.getDefault())
    return dateFormat.format(date)
}

fun parseDate(text: String): Date {
    val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
    return sdf.parse(text)
}

