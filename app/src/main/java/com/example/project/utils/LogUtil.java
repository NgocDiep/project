package com.example.project.utils;

import android.util.Log;

import com.example.project.BuildConfig;

public class LogUtil {
	
	public static final boolean IS_DEBUG = BuildConfig.DEBUG;
	
	
	public static void debug(String tag, int value){
		if (IS_DEBUG) Log.d(tag, String.valueOf(value));
	}
	
	public static void debug(String tag, float value){
		if (IS_DEBUG) Log.d(tag, String.valueOf(value));
	}

	public static void debug(String tag, boolean value){
		if (IS_DEBUG) Log.d(tag, String.valueOf(value));
	}
	
	public static void debug(String tag, String message){
		if (IS_DEBUG) Log.d(tag, message);
	}
}
