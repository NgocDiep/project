package com.example.project.model

data class QuoteItem(
    val id: Int,
    val quote_id: Int,
    val product_id: Int,
    val product_name: String,
    val product_code: String,
    val product_discount_rate: String,
    val qty: String,
    val price: String,
    val created_at: String,
    val updated_at: String
)