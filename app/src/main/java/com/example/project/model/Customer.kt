package com.example.project.model

data class Customer (
    val customer_id: Int?,
    val customer_name: String?,
    val customer_phone: String?,
    val customer_point: String?,
    val customer_type: String,
    val customer_total_amount: String,
    val customer_avatar: String
)