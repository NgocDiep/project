package com.example.project.model

data class Product (
//    val stt: Int,
    val product_id: Int,
    var product_name: String,
    var product_code: String,
    var product_qty: String,
//    val product_qty_pending: String,
//    val product_qty_available: String,
    var product_price: String,
    var product_unit: String,
    val unit: String,
    var product_discount_rate: String,
    //price product after discount
    val product_discount_price: String,
    val product_medias: List<String>,
    var medias: List<String>
)