package com.example.project.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserData(
    val username: String,
    val full_name: String,
    val phone: String,
    val email: String,
    val address: String,
    val id: Int,
    val token: String,
    val avatar: String
)

