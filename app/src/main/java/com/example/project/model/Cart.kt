package com.example.project.model

data class Cart(
    val id: Int,
    val user_id: Int,
    val user_name: String,
    val customer_id: Int,
    val customer_name: String,
    val customer_phone: String,
    val total_amount: Long,
    val shipping_address: String,
    val note: String,
    val discount_amount: String,
    val total_amount_receive: Long,
    val status: String,
    val created_at: String,
    val updated_at: String,
    val customer_point: Int,
    val quote_item: ArrayList<QuoteItem>
)