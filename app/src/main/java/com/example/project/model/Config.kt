package com.example.project.model

data class Config(
    val type: Int?,
    val min_amount: String?,
    val discount_amount: String?,
    val amount: String?,
    val user_id: String
)