package com.example.project.model

data class HistoryOrder(
    val id: Int,
    val customer_name: String?,
    val customer_phone: String?,
    val total_amount: String,
    val total_amount_receive: String,
    val discount_amount: String?,
    val point_used: String,
    val created_at: String,
    val item: ArrayList<QuoteItem>
)