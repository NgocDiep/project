package com.example.project.http

import com.google.gson.annotations.SerializedName

class ServiceResultNoData {
    @SerializedName("code")
    var code = ""

    @SerializedName("status")
    var status = ""

    @SerializedName("message")
    var message = ""
}