package com.example.project.http

import com.example.project.constraint.Constraint.Companion.ADD_CUSTOMER
import com.example.project.constraint.Constraint.Companion.ADD_PRODUCT
import com.example.project.constraint.Constraint.Companion.ADD_TO_CART
import com.example.project.constraint.Constraint.Companion.CREATE_CONFIG_SWITCH_POINT
import com.example.project.constraint.Constraint.Companion.CREATE_CONFIG_VIP
import com.example.project.constraint.Constraint.Companion.CREATE_DISCOUNT_MEMBER
import com.example.project.constraint.Constraint.Companion.DELETE_QUOTE_ITEM
import com.example.project.constraint.Constraint.Companion.EDIT_CUSTOMER
import com.example.project.constraint.Constraint.Companion.EDIT_PRODUCT
import com.example.project.constraint.Constraint.Companion.GET_CONFIG_SWITCH_POINT
import com.example.project.constraint.Constraint.Companion.GET_CONFIG_VIP
import com.example.project.constraint.Constraint.Companion.GET_DISCOUNT_MEMBER
import com.example.project.constraint.Constraint.Companion.GET_HISTORY_ORDER_DETAIL
import com.example.project.constraint.Constraint.Companion.GET_LIST_CUSTOMER
import com.example.project.constraint.Constraint.Companion.GET_LIST_HISTORY_ORDER
import com.example.project.constraint.Constraint.Companion.GET_LIST_PRODUCT
import com.example.project.constraint.Constraint.Companion.LOGIN
import com.example.project.constraint.Constraint.Companion.PAY
import com.example.project.constraint.Constraint.Companion.REGISTER
import com.example.project.model.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface APIService {
    @POST(LOGIN)
    fun login(@Body body: Map<String, String>): Observable<ServiceResult<UserData>>

    @Multipart
    @POST(REGISTER)
    fun register(
        @Part file: MultipartBody.Part?, @Part("username") username: RequestBody,
        @Part("password") password: RequestBody, @Part("password_confirmation") password_confirmation: RequestBody,
        @Part("full_name") full_name: RequestBody, @Part("address") address: RequestBody,
        @Part("phone") phone: RequestBody, @Part("email") email: RequestBody
    ): Observable<ServiceResult<UserData>>

    @Multipart
    @POST(ADD_PRODUCT)
    fun addProduct(
        @Part("name") name: RequestBody, @Part("code") code: RequestBody,
        @Part("qty") qty: RequestBody, @Part("price") price: RequestBody,
        @Part("discount_rate") discount_rate: RequestBody, @Part("unit") unit: RequestBody,
        @Part listFile: List<MultipartBody.Part>
    ): Observable<ServiceResult<Product>>

    @Multipart
    @POST(EDIT_PRODUCT)
    fun editProduct(
        @Part("id") id: RequestBody,
        @Part("name") name: RequestBody, @Part("code") code: RequestBody,
        @Part("qty") qty: RequestBody, @Part("price") price: RequestBody,
        @Part("discount_rate") discount_rate: RequestBody, @Part("unit") unit: RequestBody,
        @Part listFile: List<MultipartBody.Part>
    ): Observable<ServiceResult<Product>>

    @POST(GET_LIST_PRODUCT)
    fun getListProduct(@Body body: Map<String, String>): Observable<ServiceResult<ArrayList<Product>>>

    @POST(ADD_TO_CART)
    fun addToCart(@Body body: Map<String, String>): Observable<ServiceResult<Cart>>

    @POST(PAY)
    fun pay(@Body body: Map<String, String>): Observable<ServiceResultNoData>

    @POST(DELETE_QUOTE_ITEM)
    fun deleteQuoteItem(@Body body: Map<String, String>): Observable<ServiceResult<Cart>>

    @POST(GET_LIST_CUSTOMER)
    fun getListCustomer(@Body body: Map<String, String>): Observable<ServiceResult<ArrayList<Customer>>>

    @Multipart
    @POST(EDIT_CUSTOMER)
    fun editCustomer(
        @Part("customer_name") id: RequestBody, @Part("customer_phone") customer_phone: RequestBody,
        @Part("customer_point") customer_point: RequestBody, @Part("customer_id") customer_id: RequestBody
    ): Observable<ServiceResult<Customer>>

    @Multipart
    @POST(ADD_CUSTOMER)
    fun addCustomer(
        @Part("customer_name") customer_name: RequestBody,
        @Part("customer_phone") customer_phone: RequestBody
        ): Observable<ServiceResult<Customer>>

    @POST(GET_DISCOUNT_MEMBER)
    fun getDiscountMember(@Body body: Map<String, String>): Observable<ServiceResult<ArrayList<Config>>>

    @POST(CREATE_DISCOUNT_MEMBER)
    fun createDiscountMember(@Body body: Map<String, String>): Observable<ServiceResultNoData>

    @GET(GET_CONFIG_VIP)
    fun getConfigVip(): Observable<ServiceResult<Config>>

    @POST(CREATE_CONFIG_VIP)
    fun createConfigVip(@Body body: Map<String, String>): Observable<ServiceResult<Config>>

    @GET(GET_CONFIG_SWITCH_POINT)
    fun getConfigSwitchPoint(): Observable<ServiceResult<Config>>

    @POST(CREATE_CONFIG_SWITCH_POINT)
    fun createConfigSwitchPoint(@Body body: Map<String, String>): Observable<ServiceResult<Config>>

    @POST(GET_LIST_HISTORY_ORDER)
    fun getListHistoryOrder(@Body body: Map<String, String>): Observable<ServiceResult<ArrayList<HistoryOrder>>>

    @GET(GET_HISTORY_ORDER_DETAIL)
    fun getHistoryOrderDetail(@Path("id") id: Int): Observable<ServiceResult<HistoryOrder>>
}