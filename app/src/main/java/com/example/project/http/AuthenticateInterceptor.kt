package com.example.project.http

import android.text.TextUtils
import com.example.project.datalocal.Preferences
import okhttp3.Interceptor
import okhttp3.Response

class AuthenticateInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val builder = request.headers().newBuilder()
        builder.add("Content-Type", "application/json")
        if (!TextUtils.isEmpty(Preferences.getInstance().getUsername())) {
            builder.add("username", "${Preferences.getInstance().getUsername()}")
        }
        if (!TextUtils.isEmpty(Preferences.getInstance().getToken())) {
            builder.add("Authorization", "Bearer ${Preferences.getInstance().getToken()}")
        }
        val headers = builder.build()
        val newRequest = request.newBuilder().headers(headers).build()
        val response = chain.proceed(newRequest)
        return response
    }
}