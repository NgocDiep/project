package com.example.project.activity.codescannerview

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.project.constraint.Constraint
import com.example.project.constraint.Constraint.Companion.KEY_CODE
import com.example.project.constraint.Constraint.Companion.currentCode
import com.example.project.constraint.Constraint.Companion.loadProductList
import com.example.project.utils.CheckPermission.MY_PERMISSIONS_REQUEST_CAMERA
import com.example.project.utils.CheckPermission.checkPermissionCAMERA
import com.example.project.utils.CheckPermission.showAlertPermissionCAMERA
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView

class ZxingScannerActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {
    private lateinit var mScannerView: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mScannerView = ZXingScannerView(this)
        try {
            if (checkPermissionCAMERA(this)) {
                setContentView(mScannerView)
            }
        } catch (e: Throwable) {
            LogUtil.debug("__err_camera__", e.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        mScannerView.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView.startCamera() // Start camera on resume
    }

    override fun onPause() {
        super.onPause()
        mScannerView.stopCamera() // Stop camera on pause
    }

    override fun handleResult(rawResult: Result?) {
        MessageBox().showShortToast(this, rawResult!!.getText())
        currentCode = rawResult!!.getText()
        val intent = Intent()
        intent.putExtra(KEY_CODE, rawResult!!.getText())
        setResult(Activity.RESULT_OK, intent)
        Constraint.selectedTabResume == "product"
        loadProductList = false
        finish()
        mScannerView.resumeCameraPreview(this)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setContentView(mScannerView)
            } else {
                showAlertPermissionCAMERA(this)
            }
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }
}
