package com.example.project.activity.register

import android.content.Context
import android.content.DialogInterface
import android.net.Uri
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project.R
import com.example.project.http.API
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import com.example.project.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.regex.Pattern

class VMRegisterActivity : ViewModel() {
    private lateinit var context: Context
    lateinit var loading: MutableLiveData<Int>
    lateinit var registerSuccess: MutableLiveData<Boolean>

    fun init(context: Context) {
        this.context = context
        this.loading = MutableLiveData(View.GONE)
        this.registerSuccess = MutableLiveData()
    }

    fun register(
        avatarPath: String?, username: String, password: String, password_confirmation: String,
        full_name: String, address: String, phone: String, email: String
    ) {
        //validate data
        if (!Utils.isNullOrEmpty(avatarPath)) {
            val avatarFile: File? = File(avatarPath!!)
            if (avatarFile == null) {
                MessageBox().showLongToast(context, context.getString(R.string.err))
                return
            }
        } else {
            MessageBox().showLongToast(context, "Bạn cần chọn ảnh đại diện")
            return
        }

        if (Utils.isNullOrEmpty(username) || Utils.isNullOrEmpty(password)
            || Utils.isNullOrEmpty(password_confirmation) || Utils.isNullOrEmpty(full_name)
            || Utils.isNullOrEmpty(address) || Utils.isNullOrEmpty(phone)
            || Utils.isNullOrEmpty(email)
        ) {
            MessageBox().showLongToast(context, context.getString(R.string.require_fill_full_inf))
            return
        }

        var regularExpression = "[a-zA-Z][a-zA-Z0-9_-]{2,}$"
        var pattern = Pattern.compile(regularExpression)
        var matcher = pattern.matcher(username.trim({ it <= ' ' }))
        if (username.length < 3) {
            MessageBox().showLongToast(context, context.getString(R.string.username_longer_3))
            return
        } else if (!matcher.matches()) {
            MessageBox().showLongToast(context, context.getString(R.string.invalid_username))
            return
        }

        if (password.length < 6) {
            MessageBox().showLongToast(context, context.getString(R.string.password_require_lenght))
            return
        }

        if (!password_confirmation.equals(password)) {
            MessageBox().showLongToast(
                context,
                context.getString(R.string.false_password_confirmation)
            )
            return
        }

        if (!"".equals(email, ignoreCase = true) && !Utils.isEmailValid(email)) {
            MessageBox().showLongToast(context, context.getString(R.string.invalid_email))
            return
        }

        regularExpression = "^0[1-9]{1}[0-9]{8,9}$"
        pattern = Pattern.compile(regularExpression)
        matcher = pattern.matcher(phone.trim({ it <= ' ' }))
        if (!matcher.matches()) {
            MessageBox().showLongToast(context, context.getString(R.string.invalid_phone))
            return
        }

        loading.value = View.VISIBLE
        val c_username = RequestBody.create(MediaType.parse("text/plain"), username)
        val c_password = RequestBody.create(MediaType.parse("text/plain"), password)
        val c_password_confirmation =
            RequestBody.create(MediaType.parse("text/plain"), password_confirmation)
        val c_full_name = RequestBody.create(MediaType.parse("text/plain"), full_name)
        val c_address = RequestBody.create(MediaType.parse("text/plain"), address)
        val c_phone = RequestBody.create(MediaType.parse("text/plain"), phone)
        val c_email = RequestBody.create(MediaType.parse("text/plain"), email)

        API.apiService.register(
            convertToFilePath(avatarPath!!), c_username, c_password, c_password_confirmation,
            c_full_name, c_address, c_phone, c_email
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loading.value = View.GONE
                if (it.status == "success") {
                    MessageBox().showAlert(
                        context,
                        it.message,
                        context.getString(R.string.ok),
                        object : DialogInterface.OnClickListener {
                            override fun onClick(p0: DialogInterface?, p1: Int) {
                                registerSuccess.value = true
                            }
                        })
                } else
                    MessageBox().showAlert(context, it.message, context.getString(R.string.ok))
            }, {
                loading.value = View.GONE
                LogUtil.debug("__register__", it.toString())
                MessageBox().showAlert(
                    context,
                    context.getString(R.string.err_internet),
                    context.getString(R.string.ok)
                )
            }).isDisposed
    }

    fun convertToFilePath(avatarPath: String): MultipartBody.Part? {
        return try {
            val uri = avatarPath
            var mediaType: String? = context.getContentResolver().getType(Uri.parse(uri))
            if (mediaType == null) {
                if (uri.endsWith("jpeg")) mediaType = "image/jpeg"
                if (uri.endsWith("jpg")) mediaType = "image/jpg"
                if (uri.endsWith("png")) mediaType = "image/png"
            }
            val avatarFile = File(avatarPath)
            val avatar: RequestBody =
                RequestBody.create(MediaType.parse(mediaType), avatarFile)
            MultipartBody.Part.createFormData("avatar", avatarFile.getName(), avatar)
        } catch (e: Throwable) {
            null
        }
    }
}