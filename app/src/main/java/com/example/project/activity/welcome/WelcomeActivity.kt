package com.example.project.activity.welcome

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.project.R
import com.example.project.datalocal.Preferences
import com.example.project.navigation.MyNavigation
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import kotlinx.android.synthetic.main.activity_welcome.*


class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        Preferences.init(this)
        viewflipper.displayedChild = 0
        if (Preferences.getInstance().getIsFirst()) {
            Handler().postDelayed({
                viewflipper.displayedChild = 1
                setSlider()
                event()
            }, 1500)
        } else {
            Handler().postDelayed({
                MyNavigation.getInstance().goToLogin(this)
            }, 1500)
        }
    }

    fun event() {
        btn_start.setOnClickListener {
            MyNavigation.getInstance().goToLogin(this@WelcomeActivity)
        }
    }

    private fun setSlider() {
        Preferences.getInstance().saveIsFirst(false)
        val adapter = SliderAdapter(this)

        imageSlider?.apply {
            sliderAdapter = adapter
            setIndicatorAnimation(IndicatorAnimations.WORM)
            setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
            autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
            indicatorSelectedColor = Color.WHITE
            indicatorUnselectedColor = Color.GRAY
            scrollTimeInSec = 10
//            startAutoCycle()
        }
    }
}
