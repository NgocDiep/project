package com.example.project.activity.welcome;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.project.R;
import com.smarteist.autoimageslider.SliderViewAdapter;

public class SliderAdapter extends
        SliderViewAdapter<SliderAdapter.SliderAdapterVH> {

    private Context context;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_image_slider, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {
        switch (position) {
            case 0:
                Glide.with(viewHolder.itemView)
                        .load(R.drawable.splash3)
                        .into(viewHolder.imageViewBackground);
                viewHolder.tvTitle.setText(R.string.magage_product);
                viewHolder.tvBody.setText(R.string.sub_manage_product);
                break;
            case 1:
                Glide.with(viewHolder.itemView)
                        .load(R.drawable.scan_code)
                        .into(viewHolder.imageViewBackground);
                viewHolder.tvTitle.setText(R.string.payment_easy);
                viewHolder.tvBody.setText(R.string.sub_payment_easy);
                break;
            case 2:
                Glide.with(viewHolder.itemView)
                        .load(R.drawable.gold)
                        .into(viewHolder.imageViewBackground);
                viewHolder.tvTitle.setText(R.string.manage_revuene);
                viewHolder.tvBody.setText(R.string.sub_manage_revuene);
                break;
            case 3:
                Glide.with(viewHolder.itemView)
                        .load(R.drawable.gift)
                        .into(viewHolder.imageViewBackground);
                viewHolder.tvTitle.setText(R.string.manage_customer);
                viewHolder.tvBody.setText(R.string.sub_manage_customer);
                break;
            default:
                Glide.with(viewHolder.itemView)
                        .load(R.drawable.splash3)
                        .into(viewHolder.imageViewBackground);
                viewHolder.tvTitle.setText(R.string.magage_product);
                viewHolder.tvBody.setText(R.string.sub_manage_product);
                break;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    static class SliderAdapterVH extends SliderViewAdapter.ViewHolder {
        View itemView;
        ImageView imageViewBackground;
        TextView tvTitle, tvBody;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            this.itemView = itemView;
            imageViewBackground = itemView.findViewById(R.id.iv_image);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvBody = itemView.findViewById(R.id.tv_body);
        }
    }

}
