package com.example.project.activity.login

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project.R
import com.example.project.http.API
import com.example.project.model.UserData
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class VMLoginActivity : ViewModel() {
    private lateinit var context: Context
    private lateinit var body: HashMap<String, String>
    lateinit var loading: MutableLiveData<Int>
    lateinit var loginSuccess: MutableLiveData<UserData>

    fun init(context: Context) {
        this.context = context
        this.loading = MutableLiveData(View.GONE)
        this.loginSuccess = MutableLiveData()
    }

    fun login(username: String, password: String) {
        loading.value = View.VISIBLE
        body = hashMapOf(
            "username" to username,
            "password" to password
        )

        API.apiService.login(body).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loading.value = View.GONE
                if (it.status == "success") {
                    if (it.result != null) {
                        loginSuccess.value = it.result!!
                    }
                } else {
                    MessageBox().showLongToast(context, it.message)
                }
            }, {
                MessageBox().showLongToast(context, context.getString(R.string.err_internet))
                LogUtil.debug("__login__", it.toString())
                loading.value = View.GONE
            }).isDisposed
    }
}