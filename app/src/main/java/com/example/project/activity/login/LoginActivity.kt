package com.example.project.activity.login

import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.project.R
import com.example.project.base.BaseActivity
import com.example.project.datalocal.Preferences
import com.example.project.navigation.MyNavigation
import com.example.project.utils.MessageBox
import com.example.project.utils.Utils
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {
    private lateinit var viewModel: VMLoginActivity
    val MODE_EYE_VISIBLE = 0
    val MODE_EYE_INVISIBLE = 1
    var username: String? = ""
    var password: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        if (Preferences.getInstance().getToken() != null && Preferences.getInstance().getUsername() != null
            && Preferences.getInstance().getPassword() != null) {
            MyNavigation.getInstance().goToHome(this@LoginActivity)
        } else {
            init()
            event()
            dataListener()
        }
    }

    fun init() {
        viewModel = ViewModelProviders.of(this).get(VMLoginActivity::class.java)
        viewModel.init(this)

        username = Preferences.getInstance().getUsername()
        password = Preferences.getInstance().getPassword()
        checkbox.isChecked = false
        if (username != null && password != null) {
            edt_username.setText(username)
            edt_password.setText(password)
            checkbox.isChecked = true
        }
    }

    fun event() {
        btn_login.setOnClickListener {
            username = edt_username.text.toString()
            password = edt_password.text.toString()
            if (Utils.isNullOrEmpty(username) || Utils.isNullOrEmpty(password)) {
                MessageBox().showLongToast(
                    this@LoginActivity,
                    getString(R.string.require_fill_full_inf)
                )
            } else {
                viewModel.login(username!!, password!!)
            }
        }

        btn_register.setOnClickListener {
            MyNavigation.getInstance().goToRegister(this@LoginActivity)
//            MyNavigation.getInstance().goToZxingCodeScanner(this@LoginActivity)
        }

        viewFlipperEye.setOnClickListener { viewPassword() }
    }

    private fun viewPassword() {
        if (viewFlipperEye.getDisplayedChild() == MODE_EYE_VISIBLE) {
            viewFlipperEye.setDisplayedChild(MODE_EYE_INVISIBLE)
            edt_password.setTransformationMethod(null)
        } else {
            viewFlipperEye.setDisplayedChild(MODE_EYE_VISIBLE)
            edt_password.setTransformationMethod(PasswordTransformationMethod())
        }
    }

    fun dataListener() {
        viewModel.loading.observe(this, Observer {
            if (it == View.VISIBLE) {
                showLoading()
            } else if (it == View.GONE) {
                dismissLoading()
            }
        })

        viewModel.loginSuccess.observe(this, Observer {
            if (it != null) {
                if (checkbox.isChecked) {
                    Preferences.getInstance().savePassword(edt_password.text.toString())
                    Preferences.getInstance().saveAccount(
                        it.username, it.full_name, it.phone,
                        it.email, it.address, it.avatar
                    )
                } else {
                    Preferences.getInstance().clearAccount()
                }
                Preferences.getInstance().saveAccount(it.username, it.token)
                MyNavigation.getInstance().goToHome(this@LoginActivity)
            }
        })
    }
}
