package com.example.project.activity.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.example.project.R
import com.example.project.base.BaseActivity
import com.example.project.constraint.Constraint
import com.example.project.constraint.Constraint.Companion.codeProductAddToCart
import com.example.project.constraint.Constraint.Companion.currentCode
import com.example.project.datalocal.Preferences
import com.example.project.navigation.MyNavigation

class HomeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        MyNavigation.init(this)

        Preferences.init(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constraint.KEY_SCAN_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                currentCode = data.getStringExtra(Constraint.KEY_CODE)!!
            }
        }

        if (requestCode == Constraint.KEY_SEARCH_BY_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                currentCode = data.getStringExtra(Constraint.KEY_CODE)!!
            }
        }

        if (requestCode == Constraint.KEY_SEARCH_TO_PAY && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                codeProductAddToCart = data.getStringExtra(Constraint.KEY_CODE)!!
                currentCode = ""
            }
        }
    }
}
