package com.example.project.activity.register

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.provider.MediaStore
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.example.project.R
import com.example.project.base.BaseActivity
import com.example.project.dialog.LDLMPhotoDialog
import com.example.project.utils.CheckPermission.checkPermissionCAMERA
import com.example.project.utils.CheckPermission.checkPermissionREAD_EXTERNAL_STORAGE
import com.example.project.utils.CheckPermission.showAlertPermissionCAMERA
import com.example.project.utils.CheckPermission.showAlertPermissionREAD_EXTERNAL_STORAGE
import com.example.project.utils.FileImage
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import kotlinx.android.synthetic.main.activity_register.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class RegisterActivity : BaseActivity() {
    lateinit var photoDialog: LDLMPhotoDialog
    private val REQUEST_TAKE_PHOTO = 1
    private val REQUEST_PICK_PHOTO = 10
    var currentPhotoPath: String? = null
    private lateinit var viewModel: VMRegisterActivity
//    var avatarFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
        event()
        dataListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        deleteCache(this@RegisterActivity)
    }

    private fun init() {
        viewModel = ViewModelProviders.of(this).get(VMRegisterActivity::class.java)
        viewModel.init(this)
    }

    private fun event() {
        navbar.setOnClickListener { finish() }

        rl_avatar.setOnClickListener {
            FileImage.deleteFile(currentPhotoPath)
            photoDialog = LDLMPhotoDialog(this@RegisterActivity)
            photoDialog.showDialog(onGetPhotoFromCamera, onGetPhotoFromLib)
        }

        btn_register.setOnClickListener {
            viewModel.register(
                currentPhotoPath,
                edt_username.text.toString(),
                edt_password.text.toString(),
                edt_password_confirmation.text.toString(),
                edt_fullname.text.toString(),
                edt_address.text.toString(),
                edt_phone.text.toString(),
                edt_email.text.toString()
            )
        }
    }

    private fun closePopup() {
        if (photoDialog != null && photoDialog.isShowing()) {
            photoDialog.dismiss()
        }
    }

    private val onGetPhotoFromCamera = View.OnClickListener {
        dispatchTakePictureIntent()
        closePopup()
    }

    private fun dispatchTakePictureIntent() {
        if (checkPermissionCAMERA(this@RegisterActivity)) {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            var photoFile: File? = null

            try {
                val builder = VmPolicy.Builder()
                StrictMode.setVmPolicy(builder.build())
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (takePictureIntent.resolveActivity(this@RegisterActivity.getPackageManager()) != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile))
                startActivityForResult(
                    takePictureIntent,
                    REQUEST_TAKE_PHOTO
                )
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File? { // Create an image file name
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir: File? =
            this@RegisterActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        var image: File? = null
        if (storageDir != null) image = File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",  /* suffix */
            storageDir /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image!!.absolutePath
        return image
    }

    private val onGetPhotoFromLib = View.OnClickListener {
        pickImageFromGallery()
        closePopup()
    }

    private fun pickImageFromGallery() {
        if (Build.VERSION.SDK_INT <= 19) {
            val i = Intent()
            i.type = "image/*"
            i.action = Intent.ACTION_GET_CONTENT
            i.addCategory(Intent.CATEGORY_OPENABLE)
            startActivityForResult(
                i,
                REQUEST_PICK_PHOTO
            )
        } else if (Build.VERSION.SDK_INT > 19) {
            if (checkPermissionREAD_EXTERNAL_STORAGE(this@RegisterActivity)) {
                val i = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )
                startActivityForResult(
                    i,
                    REQUEST_PICK_PHOTO
                )
            } else {
                MessageBox().showLongToast(
                    this@RegisterActivity,
                    getString(R.string.permission_read_lib)
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_TAKE_PHOTO -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent()
                } else showAlertPermissionCAMERA(this@RegisterActivity)
            }
            REQUEST_PICK_PHOTO -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickImageFromGallery()
                } else showAlertPermissionREAD_EXTERNAL_STORAGE(this@RegisterActivity)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
                if (FileImage.checkFileImageNull(currentPhotoPath)) {
                    iv_avatar.setImageURI(Uri.parse(currentPhotoPath))
//                    avatarFile = File(currentPhotoPath)
                } else MessageBox().showLongToast(this@RegisterActivity, getString(R.string.err))
            }
            if (requestCode == REQUEST_PICK_PHOTO && resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    val uri = data.data
                    // Get the file path from Uri
                    val path: String =
                        com.example.project.utils.FileUtils.getPath(this@RegisterActivity, uri)
                    if (path != null && com.example.project.utils.FileUtils.isLocal(path)) {
                        try {
                            currentPhotoPath = path
                            if (FileImage.checkFileImageNull(currentPhotoPath)) {
                                iv_avatar.setImageURI(Uri.parse(currentPhotoPath))
//                                avatarFile = File(currentPhotoPath)
                            } else MessageBox().showLongToast(
                                this@RegisterActivity,
                                getString(R.string.err)
                            )
                        } catch (e: Throwable) {
                            LogUtil.debug("__err_avatar__", e.toString())
                            MessageBox().showLongToast(
                                this@RegisterActivity,
                                getString(R.string.err)
                            )
                        }
                    }
                }
            }
        } catch (e: Throwable) {
            LogUtil.debug("__err_avatar__", e.toString())
            MessageBox().showLongToast(
                this@RegisterActivity,
                getString(R.string.err)
            )
        }
    }

    fun dataListener() {
        viewModel.loading.observe(this, androidx.lifecycle.Observer {
            if (it == View.VISIBLE) {
                showLoading()
            } else if (it == View.GONE) {
                dismissLoading()
            }
        })

        viewModel.registerSuccess.observe(this, androidx.lifecycle.Observer {
            if (it) {
                onBackPressed()
            }
        })
    }
}
