package com.example.project.datalocal

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.example.project.R

class Preferences private constructor(val context: Context) {
    private val appName = context.getString(R.string.app_name)
    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(appName, Context.MODE_PRIVATE)

    companion object {
        @SuppressLint("StaticFieldLeak")
        private var instance: Preferences? = null

        fun getInstance(): Preferences {
            if (instance == null) {
                throw IllegalAccessException("Preferences must be initialized in Application class first.")
            }
            return instance as Preferences
        }

        fun init(context: Context) {
            instance = Preferences(context)
        }
    }

    fun saveAccount(username: String, token: String) {
        sharedPreferences.edit().putString("username", username).putString("token", token).apply()
    }

    fun savePassword(password: String) {
        sharedPreferences.edit().putString("password", password).apply()
    }

    fun saveAccount(
        username: String,
        fullname: String,
        phone: String,
        email: String,
        address: String,
        avatar: String
    ) {
        sharedPreferences.edit().putString("username", username).putString("fullname", fullname)
            .putString("phone", phone).putString("email", email).putString("address", address)
            .putString("avatar", avatar)
            .apply()
    }

    fun getFullname(): String? {
        return sharedPreferences.getString("fullname", null)
    }

    fun getAvatar(): String? {
        return sharedPreferences.getString("avatar", null)
    }

    fun getPhone(): String? {
        return sharedPreferences.getString("phone", null)
    }

    fun getAddress(): String? {
        return sharedPreferences.getString("address", null)
    }

    fun getEmail(): String? {
        return sharedPreferences.getString("email", null)
    }

    fun getUsername(): String? {
        return sharedPreferences.getString("username", null)
    }

    fun getPassword(): String? {
        return sharedPreferences.getString("password", null)
    }

    fun clearAccount() {
        sharedPreferences.edit().remove("username").remove("password").remove("token").apply()
    }

    fun getToken(): String? {
        return sharedPreferences.getString("token", null)
    }

    fun saveIsFirst(isFirst: Boolean) {
        sharedPreferences.edit().putBoolean("isFirst", isFirst).apply()
    }

    fun getIsFirst(): Boolean {
        return sharedPreferences.getBoolean("isFirst", true)
    }
}