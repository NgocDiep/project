package com.example.project.constraint

import com.example.project.model.*

class Constraint {
    companion object {
        const val LOGIN = "/api/login"
        const val REGISTER = "/api/register"
        const val ADD_PRODUCT = "/api/product/add"
        const val EDIT_PRODUCT = "/api/product/edit"
        const val GET_LIST_PRODUCT = "/api/product/list"
        const val ADD_TO_CART = "/api/quote/create"
        const val PAY = "/api/order/create"
        const val DELETE_QUOTE_ITEM = "/api/quote/delete-quote-item"
        const val GET_LIST_CUSTOMER = "/api/customer/list"
        const val EDIT_CUSTOMER = "/api/customer/edit"
        const val ADD_CUSTOMER = "/api/customer/add"
        const val GET_DISCOUNT_MEMBER = "/api/config-discount-member/list"
        const val CREATE_DISCOUNT_MEMBER = "/api/config-discount-member/create"
        const val GET_CONFIG_VIP = "/api/config-vip/detail"
        const val CREATE_CONFIG_VIP = "/api/config-vip/create"
        const val GET_CONFIG_SWITCH_POINT = "/api/config-switch-point/detail"
        const val CREATE_CONFIG_SWITCH_POINT = "/api/config-switch-point/create"
        const val GET_LIST_HISTORY_ORDER = "/api/order/list"
        const val GET_HISTORY_ORDER_DETAIL = "/api/order/detail/{id}"

        var KEY_SCAN_CODE = 9999
        var KEY_SEARCH_BY_CODE = 8888
        var KEY_SEARCH_TO_PAY = 7777
        var KEY_CODE = "KEY_CODE"
        var currentCode = ""
        var codeProductAddToCart = ""
        var currentProduct: Product? = null
        var KEY_PRODUCT_DETAIL = "add"
        var currentCart: Cart? = null
        var selectedTabResume = ""
        var noCustomer = false
        var currentConfig: Config? = null
        var currentCustomer: Customer? = null
        var currentDiscountType = ""
        var currentType = ""
        var currentHistoryOrder: HistoryOrder? = null
        var removeDeleteIcon: Boolean = false
        //biến này để check người dùng đã chọn đơn hàng đc thanh toán không bởi thành viên cửa hàng
        var cartNoCustomer: Boolean = false
        //Set thêm 1 biến quy định có load sản phẩm hay không
        var loadProductList = false
    }
}