package vn.dms.sharkforsale.dmsforcustomer.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.project.utils.ProcessDialogCommon

class VMGeneral : ViewModel() {
    fun showLoading(context: Context?, cancelable: Boolean) {
        ProcessDialogCommon.showdialog(context, cancelable)
    }

    fun dismissLoading() {
        ProcessDialogCommon.closeDialog()
    }
}