package com.example.project.navigation

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.project.R
import com.example.project.activity.codescannerview.ZxingScannerActivity
import com.example.project.activity.home.HomeActivity
import com.example.project.activity.login.LoginActivity
import com.example.project.activity.register.RegisterActivity
import com.example.project.constraint.Constraint.Companion.KEY_SCAN_CODE
import com.example.project.constraint.Constraint.Companion.KEY_SEARCH_BY_CODE
import com.example.project.constraint.Constraint.Companion.KEY_SEARCH_TO_PAY

class MyNavigation {
    companion object {
        @SuppressLint("StaticFieldLeak")
        private var instance: MyNavigation? = null
        @SuppressLint("StaticFieldLeak")
        private var navController: NavController? = null

        fun getInstance(): MyNavigation {
            if (instance == null) {
                instance = MyNavigation()
//                throw IllegalAccessException("Preferences must be initialized in Application class first.")
            }
            return instance as MyNavigation
        }

        fun init(activity: Activity) {
            navController = Navigation.findNavController(
                activity,
                R.id.nav_host_fragment
            )
            instance = MyNavigation()
        }
    }

    fun goToZxingCodeScanner(activity: Activity) {
        val intent = Intent(activity, ZxingScannerActivity::class.java)
        activity.startActivityForResult(intent, KEY_SCAN_CODE)
    }

    fun goToZxingCodeScannerToSearch(activity: Activity) {
        val intent = Intent(activity, ZxingScannerActivity::class.java)
        activity.startActivityForResult(intent, KEY_SEARCH_BY_CODE)
    }

    fun goToZxingCodeScannerToPay(activity: Activity) {
        val intent = Intent(activity, ZxingScannerActivity::class.java)
        activity.startActivityForResult(intent, KEY_SEARCH_TO_PAY)
    }

    fun goToLogin(activity: Activity) {
        val intent = Intent(activity, LoginActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }

    fun goToRegister(activity: Activity) {
        val intent = Intent(activity, RegisterActivity::class.java)
        activity.startActivity(intent)
    }

    fun goToHome(activity: Activity) {
        val intent = Intent(activity, HomeActivity::class.java)
        activity.startActivity(intent)
    }

    fun goToAddProduct() {
        if (navController!!.currentDestination?.id == R.id.homeFragment) {
            navController!!.navigate(R.id.action_homeFragment_to_addProductFragment)
        }
    }

    fun goToEditProduct() {
        if (navController!!.currentDestination?.id == R.id.productDetailFragment) {
            navController!!.navigate(R.id.action_productDetailFragment_to_addProductFragment)
        }
    }

    fun goToProductDetail() {
        if (navController!!.currentDestination?.id == R.id.homeFragment) {
            navController!!.navigate(R.id.action_homeFragment_to_productDetailFragment)
        }
    }

    fun goToConfiguration() {
        if (navController!!.currentDestination?.id == R.id.homeFragment) {
            navController!!.navigate(R.id.action_homeFragment_to_configurationFragment)
        }
    }

    fun goToHistoryOrderDetail() {
        if (navController!!.currentDestination?.id == R.id.homeFragment) {
            navController!!.navigate(R.id.action_homeFragment_to_historyOrderDetailFragment)
        }
    }

    fun goToAccount() {
        if (navController!!.currentDestination?.id == R.id.homeFragment) {
            navController!!.navigate(R.id.action_homeFragment_to_accountFragment)
        }
    }

    fun backNumbFrag(numb: Int) {
        for (i in 1..numb) {
            navController!!.popBackStack()
        }
        navController!!.navigateUp()
    }
}