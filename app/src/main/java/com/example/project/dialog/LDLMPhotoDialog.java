package com.example.project.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;

import com.example.project.R;

public class LDLMPhotoDialog extends Dialog implements OnClickListener {
	private Button btnCancel, btnTakePhoto, btnLibrary;

	public LDLMPhotoDialog(Context ctx) {
		super(ctx);
	}

	public void showDialog(View.OnClickListener getPhotoListener, View.OnClickListener getAlbumListener) {
//		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_pick_image_ldlm);
		this.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
		this.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
		this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		this.show();
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		this.setCanceledOnTouchOutside(true);

		getViews();
		setEvents(getPhotoListener,getAlbumListener);
	}

	private void getViews() {
		btnCancel = (Button) this.findViewById(R.id.btn_cancel);
		btnTakePhoto = (Button) this.findViewById(R.id.btn_take_photo);
		btnLibrary = (Button) this.findViewById(R.id.btn_pick_photo);
	}

	private void setEvents(View.OnClickListener getPhotoListener, View.OnClickListener getAlbumListener) {
		btnCancel.setOnClickListener(this);
		btnTakePhoto.setOnClickListener(getPhotoListener);
		btnLibrary.setOnClickListener(getAlbumListener);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_cancel:
			if(this != null && isShowing()){
			this.dismiss();
			}
			break;

		default:
			break;
		}
	}
}
