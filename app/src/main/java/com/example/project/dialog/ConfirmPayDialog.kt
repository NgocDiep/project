package com.example.project.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.example.project.R
import kotlinx.android.synthetic.main.dialog_exit_app.*

class ConfirmPayDialog(context: Context, val fAccept: () -> Unit) :
    Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_exit_app)
        setCancelable(false)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        tvContent.text = "Xác nhận thanh toán đơn hàng?"

        tvCancel.setOnClickListener {
            cancel()
        }
        tvConfirm.setOnClickListener {
            cancel()
            fAccept()
        }
    }
}