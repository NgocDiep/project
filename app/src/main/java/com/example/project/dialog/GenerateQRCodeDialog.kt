package com.example.project.dialog

import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import com.example.project.R
import com.example.project.utils.GenerateQRCode
import com.example.project.utils.MessageBox
import kotlinx.android.synthetic.main.dialog_generate_code.*
import java.io.ByteArrayOutputStream


class GenerateQRCodeDialog(context: Context, val fConfirm: (text: String) -> Unit) :
    Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
    lateinit var bitmap: Bitmap
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_generate_code)
        setCancelable(false)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        tvCancel.setOnClickListener {
            cancel()
        }
        btn_close.setOnClickListener {
            cancel()
        }
        btn_generate_code.setOnClickListener {
            val generateQRCode = GenerateQRCode()
            val text = edt_string.text.toString()
            if (text.length < 10) {
                MessageBox().showLongToast(context, "Chuỗi mã hóa phải dài ít nhất 10 ký tự")
            } else {
                vf_btn.displayedChild = 1
                vf_qr_image.visibility = View.VISIBLE
                val bitmap =
                    if (generateQRCode.generateQRCode(text) != null) generateQRCode.generateQRCode(
                        text
                    ) else null
                if (bitmap != null) {
                    vf_qr_image.displayedChild = 0
                    iv_qrcode.setImageBitmap(bitmap)
                    this.bitmap = bitmap
                } else {
                    vf_qr_image.displayedChild = 1
                    vf_btn.displayedChild = 0
                }
            }
        }
        tvConfirm.setOnClickListener {
            cancel()
            fConfirm(edt_string.text.toString())
        }
        btn_share.setOnClickListener {
            if (getImageUri(context, bitmap) != null) {
                context.startActivity(
                    shareImage(
                        context,
                        getImageUri(context, bitmap).toString(),
                        getImageUri(context, bitmap)!!
                    ))
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun shareImage(context: Context, pathToImage: String?, uri: Uri): Intent? {
        val shareIntent = Intent(Intent.ACTION_SEND)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET) else shareIntent.addFlags(
            Intent.FLAG_ACTIVITY_NEW_DOCUMENT
        )
        shareIntent.type = "image/*"
        // For a file in shared storage.  For data in private storage, use a ContentProvider.
//        val uri: Uri = Uri.fromFile(context.getFileStreamPath(pathToImage))
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
        return shareIntent
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(
            inContext.contentResolver,
            inImage,
            "Title",
            null
        )
        return Uri.parse(path)
    }
}