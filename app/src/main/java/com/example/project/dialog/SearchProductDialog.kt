package com.example.project.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.example.project.R
import com.example.project.utils.MessageBox
import kotlinx.android.synthetic.main.dialog_search_product.*

class SearchProductDialog(
    context: Context,
    val fSearch: (code: String) -> Unit,
    val fScanCode: () -> Unit
) :
    Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_search_product)
        setCancelable(false)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        btn_search.setOnClickListener {
            if (edt_key.text.toString() == "") {
                MessageBox().showLongToast(context, context.getString(R.string.require_input_key))
            } else {
                fSearch(edt_key.text.toString())
            }
        }

        btn_scan.setOnClickListener {
            fScanCode()
            cancel()
        }

        fl_btn_close.setOnClickListener {
            cancel()
        }
    }
}