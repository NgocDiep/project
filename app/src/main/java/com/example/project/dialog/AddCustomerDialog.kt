package com.example.project.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.currentCustomer
import com.example.project.utils.LogUtil
import com.example.project.utils.MessageBox
import com.example.project.utils.Utils
import kotlinx.android.synthetic.main.dialog_add_customer.*
import kotlinx.android.synthetic.main.dialog_generate_code.btn_close

class AddCustomerDialog(
    context: Context,
    val fConfirm: (customer_name: String, customer_phone: String, customer_point: String?) -> Unit
) :
    Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
    var type = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_add_customer)
        setCancelable(false)
        window?.setBackgroundDrawableResource(android.R.color.transparent)

        fillData()
        btn_close.setOnClickListener {
            cancel()
        }

        btn_save_inf.setOnClickListener {
            if (Utils.isNullOrEmpty(edt_customer_name.text.toString()) || Utils.isNullOrEmpty(
                    edt_customer_phone.text.toString()
                )
            ) {
                MessageBox().showLongToast(
                    context,
                    context.getString(R.string.require_fill_full_inf)
                )
            } else if (currentCustomer != null) {
                if (edt_customer_name.text.toString() == currentCustomer!!.customer_name &&
                    edt_customer_phone.text.toString() == currentCustomer!!.customer_phone
                ) {
                    MessageBox().showLongToast(
                        context,
                        context.getString(R.string.no_change_inf)
                    )
                } else {
                    fConfirm(
                        edt_customer_name.text.toString(),
                        edt_customer_phone.text.toString(),
                        edt_customer_point.text.toString()
                    )
                }
            } else if (type == "add") {
                fConfirm(
                    edt_customer_name.text.toString(),
                    edt_customer_phone.text.toString(),
                    edt_customer_point.text.toString()
                )
            }
        }
    }

    fun fillData() {
        if (currentCustomer != null) {
            tv_title.text = "Sửa thông tin khách hàng"
            item_customer_point.visibility = View.VISIBLE
            item_customer_name.expand()
            edt_customer_name.setText(currentCustomer!!.customer_name)
            item_customer_phone.expand()
            edt_customer_phone.setText(currentCustomer!!.customer_phone)
            item_customer_point.expand()
            edt_customer_point.setText(currentCustomer!!.customer_point)
            type = "edit"
        } else {
            tv_title.text = "Thêm khách hàng"
            type = "add"
        }
    }

    override fun cancel() {
        super.cancel()
        if (type == "edit") {
            currentCustomer = null
        }
    }
}