package com.example.project.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;

import com.example.project.R;

public class InputCodeDialog extends Dialog implements OnClickListener {
    private Button btnCancel, btnScanCode, btnGenerateCode;

    public InputCodeDialog(Context ctx) {
        super(ctx);
    }

    public void showDialog(View.OnClickListener scanCodeListener, View.OnClickListener generateCodeListener) {
//		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.dialog_input_code);
        this.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        this.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.show();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        this.setCanceledOnTouchOutside(true);

        getViews();
        setEvents(scanCodeListener, generateCodeListener);
    }

    private void getViews() {
        btnCancel = (Button) this.findViewById(R.id.btn_cancel);
        btnScanCode = (Button) this.findViewById(R.id.btn_scan_code);
        btnGenerateCode = (Button) this.findViewById(R.id.btn_generate_code);
    }

    private void setEvents(View.OnClickListener scanCodeListener, View.OnClickListener generateCodeListener) {
        btnCancel.setOnClickListener(this);
        btnScanCode.setOnClickListener(scanCodeListener);
        btnGenerateCode.setOnClickListener(generateCodeListener);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.btn_cancel:
                if (this != null && isShowing()) {
                    this.dismiss();
                }
                break;

            default:
                break;
        }
    }
}
