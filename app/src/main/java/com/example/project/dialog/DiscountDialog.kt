package com.example.project.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ArrayAdapter
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.currentConfig
import com.example.project.constraint.Constraint.Companion.currentDiscountType
import com.example.project.utils.MessageBox
import com.example.project.utils.Utils
import kotlinx.android.synthetic.main.dialog_discount.*
import kotlinx.android.synthetic.main.dialog_generate_code.btn_close

class DiscountDialog(
    context: Context,
    val fConfirm: (type: String, min_amount: String, discount_amount: String) -> Unit
) :
    Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
    var listType: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_discount)
        setCancelable(false)
        window?.setBackgroundDrawableResource(android.R.color.transparent)

        fillData()
        btn_close.setOnClickListener {
            cancel()
        }
        btn_save_inf.setOnClickListener {
            if (Utils.isNullOrEmpty(edt_min_amount.text.toString()) || Utils.isNullOrEmpty(
                    edt_discount_amount.text.toString()
                )
            ) {
                MessageBox().showLongToast(
                    context,
                    context.getString(R.string.require_fill_full_inf)
                )
                return@setOnClickListener
            }
            if (currentConfig != null) {
                if (edt_min_amount.text.toString() == currentConfig!!.min_amount &&
                    edt_discount_amount.text.toString() == currentConfig!!.discount_amount
                ) {
                    MessageBox().showLongToast(
                        context,
                        context.getString(R.string.no_change_inf)
                    )
                } else {
                    fConfirm(
                        spinner_type_cus.selectedItemPosition.toString(),
                        edt_min_amount.text.toString(),
                        edt_discount_amount.text.toString()
                    )
                }
            }
            if (currentConfig == null) {
                fConfirm(
                    spinner_type_cus.selectedItemPosition.toString(),
                    edt_min_amount.text.toString(),
                    edt_discount_amount.text.toString()
                )
            }
        }
    }

    fun fillData() {
        listType.add(0, "Khách thường")
        listType.add(1, "Khách VIP")

        val spinnerArrayAdapter: ArrayAdapter<String> = ArrayAdapter(
            context, android.R.layout.simple_spinner_item,
            listType
        ) //selected item will look like a spinner set from XML

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_type_cus.setAdapter(spinnerArrayAdapter)
        if (currentDiscountType == "normal") {
            spinner_type_cus.setSelection(0)
        } else spinner_type_cus.setSelection(1)

        if (currentConfig != null) {
            if (currentConfig!!.min_amount != null &&
                currentConfig!!.discount_amount != null &&
                currentConfig!!.type != null
            ) {
                item_min_amount.expand()
                item_discount_amount.expand()
                edt_min_amount.setText(currentConfig!!.min_amount)
                edt_discount_amount.setText(currentConfig!!.discount_amount)
//            spinner_type_cus.setSelection(currentConfig!!.type!!)
            }
        } else {
            var mess = ""
            if (currentDiscountType == "normal") {
                mess = "Bạn chưa cấu hình chiết khấu cho khách thường"
            } else if (currentDiscountType == "vip") {
                mess = "Bạn chưa cấu hình chiết khấu cho khách VIP"
            } else {
                mess = context.getString(R.string.err_try_again)
            }
            MessageBox().showLongToast(
                context,
                mess
            )
        }
    }

    override fun cancel() {
        super.cancel()
        currentDiscountType = ""
        currentConfig = null
    }
}