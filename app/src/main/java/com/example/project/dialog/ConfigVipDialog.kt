package com.example.project.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.currentConfig
import com.example.project.constraint.Constraint.Companion.currentType
import com.example.project.utils.MessageBox
import com.example.project.utils.Utils
import kotlinx.android.synthetic.main.dialog_generate_code.btn_close
import kotlinx.android.synthetic.main.dialog_input_amount.*

class ConfigVipDialog(
    context: Context,
    val fConfirm: (amount: String) -> Unit
) :
    Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_input_amount)
        setCancelable(false)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        if (currentType == "config_vip") {
            tv_title.text = "Cấu hình tổng tiền lên VIP"
        } else tv_title.text = "Cấu hình số tiền đổi 1 điểm"

        fillData()
        btn_close.setOnClickListener {
            cancel()
        }
        btn_save.setOnClickListener {
            if (Utils.isNullOrEmpty(edt_amount.text.toString())) {
                MessageBox().showLongToast(
                    context,
                    context.getString(R.string.require_fill_full_inf)
                )
                return@setOnClickListener
            }
            if (currentConfig != null) {
                if (edt_amount.text.toString() == currentConfig!!.amount) {
                    MessageBox().showLongToast(
                        context,
                        context.getString(R.string.no_change_inf)
                    )
                } else fConfirm(edt_amount.text.toString())
            }
            if (currentConfig == null) {
                fConfirm(edt_amount.text.toString())
            }
        }
    }

    fun fillData() {
        if (currentConfig != null) {
            if (currentConfig!!.amount != null) {
                item_amount.expand()
                edt_amount.setText(currentConfig!!.amount)
            } else {
                var mess = ""
                if (currentType == "config_vip") {
                    mess = "Bạn chưa cấu hình tổng tiền chi tiêu lên VIP"
                } else mess = "Bạn chưa cấu hình số tiền quy đổi điểm tích lũy"
                MessageBox().showLongToast(context, mess)
            }
        } else {
            var mess = ""
            if (currentType == "config_vip") {
                mess = "Bạn chưa cấu hình tổng tiền chi tiêu lên VIP"
            } else mess = "Bạn chưa cấu hình số tiền quy đổi điểm tích lũy"
            MessageBox().showLongToast(context, mess)
        }
    }

    override fun cancel() {
        super.cancel()
        currentConfig = null
        currentType = ""
    }
}