package com.example.project.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.example.project.R
import com.example.project.utils.MessageBox
import com.example.project.utils.Utils
import kotlinx.android.synthetic.main.dialog_customer_payment.*
import kotlinx.android.synthetic.main.fragment_payment.*

class CustomerPaymentDialog(
    context: Context,
    val fSearch: (customer_phone: String) -> Unit,
    val fConfirm: (customer_name: String, customer_phone: String) -> Unit
) :
    Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_customer_payment)
        setCancelable(false)
        window?.setBackgroundDrawableResource(android.R.color.transparent)

        fl_btn_close.setOnClickListener {
            cancel()
        }

        btn_search.setOnClickListener {
            if (edt_key.text.toString() == "") {
                MessageBox().showLongToast(context, context.getString(R.string.require_input_key))
            } else {
                fSearch(edt_key.text.toString())
            }
        }

        btn_add_customer.setOnClickListener {
            ll_add_customer.visibility = View.VISIBLE
            if (Utils.isNullOrEmpty(edt_name.text.toString()) || Utils.isNullOrEmpty(
                    edt_phone.text.toString()
                )
            ) {
                MessageBox().showLongToast(
                    context,
                    context.getString(R.string.require_fill_full_inf)
                )
            } else {
                fConfirm(
                    edt_name.text.toString(),
                    edt_phone.text.toString()
                )
            }
        }
    }
}