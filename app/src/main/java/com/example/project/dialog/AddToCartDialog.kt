package com.example.project.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.bumptech.glide.Glide
import com.example.project.R
import com.example.project.constraint.Constraint.Companion.currentProduct
import com.example.project.utils.MessageBox
import com.example.project.utils.formatMoney
import kotlinx.android.synthetic.main.dialog_add_to_cart.*

class AddToCartDialog(
    context: Context,
    val fAddToCart: (qty: String) -> Unit
) :
    Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_add_to_cart)
        setCancelable(false)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        var qty = edt_amount.text.toString().toInt()

        fillData()

        iv_add.setOnClickListener {
            qty = Integer.parseInt(edt_amount.text.toString()) + 1
            edt_amount.setText((qty).toString())
        }
        iv_sub.setOnClickListener {
            if (Integer.parseInt(edt_amount.text.toString()) > 0) {
                qty = Integer.parseInt(edt_amount.text.toString()) - 1
                edt_amount.setText((qty).toString())
            }
        }

        edt_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString() != "") {
                    if (Integer.parseInt(p0.toString()) == 0) {
                        cancel()
                    } else if (Integer.parseInt(p0.toString()) > currentProduct!!.product_qty.toInt()) {
                        MessageBox().showLongToast(
                            context,
                            "Chỉ còn ${currentProduct!!.product_qty} ${currentProduct!!.product_unit}"
                        )
                        edt_amount.setText(currentProduct!!.product_qty)
                    } else {

                    }
                } else {
                    edt_amount.setText("1")
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
        fl_btn_close.setOnClickListener {
            cancel()
        }
        btn_add_to_cart.setOnClickListener {
            cancel()
            fAddToCart(edt_amount.text.toString())
        }
    }

    fun fillData() {
        if (currentProduct != null) {
            if (currentProduct!!.medias.size > 0) {
                Glide.with(context).load(currentProduct!!.medias.get(0))
                    .error(R.drawable.ic_product_active)
                    .placeholder(R.drawable.ic_product_active).into(iv_image)
            }
            tv_product_name.text = currentProduct!!.product_name
            tv_product_price.text =
                context.getString(
                    R.string.price,
                    formatMoney(currentProduct!!.product_price.toLong())
                )
        }
    }

    override fun cancel() {
        super.cancel()
        currentProduct = null
    }
}