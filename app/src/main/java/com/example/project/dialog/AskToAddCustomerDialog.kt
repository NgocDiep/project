package com.example.project.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.example.project.R
import kotlinx.android.synthetic.main.dialog_exit_app.*

class AskToAddCustomerDialog(context: Context, val fDecline: () -> Unit, val fAccept: () -> Unit) :
    Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_exit_app)
        setCancelable(false)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        tvContent.text = "Khách hàng này có muốn thanh toán với vai trò là thành viên của cửa hàng không?"
        tvConfirm.text = "Có"
        tvCancel.text = "Không"
        btn_close.visibility = View.VISIBLE
        btn_close.setOnClickListener {
            cancel()
        }
        tvCancel.setOnClickListener {
            cancel()
            fDecline()
        }
        tvConfirm.setOnClickListener {
            cancel()
            fAccept()
        }
    }
}