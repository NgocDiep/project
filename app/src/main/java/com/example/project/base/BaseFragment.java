package com.example.project.base;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.project.utils.LoadingView;
import com.example.project.utils.LogUtil;

import java.io.File;

public abstract class BaseFragment extends Fragment {
    private LoadingView loadingView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            loadingView = new LoadingView(getContext());
        } catch (Exception e) {
            LogUtil.debug("__base_frag__", e.getMessage());
        }
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void showLoading() {
        try {
            loadingView.show();
        } catch (Exception e) {

        }
    }

    public void dismissLoading() {
        try {
            loadingView.cancel();
        } catch (Exception e) {

        }
    }
}
